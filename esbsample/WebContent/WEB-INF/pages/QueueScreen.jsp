<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<style type="text/css">
table {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	margin: 0px auto;
}

#result {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	color: orange;
}

#canc {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	color: red;
}

.amountDiv {
	text-align: center;
	padding: 5px;
}

label, input {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	padding: 5px 10px;
}

#refreshID {
	color: white;
	background: blue;
	padding: 5px 10px;
}

caption {
	text-align: left;
	color: silver;
	font-weight: bold;
	text-transform: uppercase;
	padding: 5px;
}

thead {
	background: SteelBlue;
	color: white;
}

tbody tr:nth-child(even) {
	background: WhiteSmoke;
}

tfoot {
	background: #e0e0d1;
	color: white;
	text-align: right;
}
</style>
<script  type="text/javascript" src="../js-lib/jquery-1.12.3.js"></script>
<script src="../js/QueueScreen.js"></script>
<title>DFD Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<table>
<tbody>
<tr>
<td>Overall Queue</td><td>:</td>
<td><input  id="overallQueue" type="text" name="overallQueue"  value = ""/></td>
</tr>
<tr><td>Erp Queue</td><td>:</td>
<td><input id="erpQueue" type="text" name="erpQueue" value = ""/></td>
</tr>
<tr><td>Crm Queue</td><td>:</td>
<td><input id="crmQueue" type="text" name="crmQueue" value =""/></td>
</tr>
<tr><td>Analytica Queue</td><td>:</td>
<td><input id="analyticaQueue" type="text" name="analyticaQueue" value =""/></td>
</tr>
<tr>
<td>DoneForTheDay ESDS1</td><td>:</td>
<td><input  id="doneForTheDayESDS1" type="text" name="doneForTheDayESDS1"  value = ""/></td>
</tr>
<tr><td>DoneForTheDay ESDS2</td><td>:</td>
<td><input id="doneForTheDayESDS2" type="text" name="doneForTheDayESDS2" value = ""/></td>
</tr>
<tr><td>DoneForTheDay ERP</td><td>:</td>
<td><input id="doneForTheDayERP" type="text" name="doneForTheDayERP" value =""/></td>
</tr>
<tr><td>Final DoneForTheDay</td><td>:</td>
<td><input id="finalDoneForTheDay" type="text" name="finalDoneForTheDay" value =""/></td>
</tr>
<tr>
<td>TxnCount ESDS</td><td>:</td>
<td><input  id="txnCountESDS" type="text" name="txnCountESDS"  value = ""/></td>
</tr>
<tr><td>CrmCountStatus</td><td>:</td>
<td><input id="crmCountStatus" type="text" name="crmCountStatus" value = ""/></td>
</tr>
<tr><td>SaleCount ESDS</td><td>:</td>
<td><input id="saleCountESDS" type="text" name="saleCountESDS" value =""/></td>
</tr>
<tr><td>Last UpdatedTime</td><td>:</td>
<td><input id="lastUpdatedTime" type="text" name="lastUpdatedTime" value =""/></td>
</tr>
<tr><td>Note</td><td>:</td>
<td><input id="note" type="text" name="note" value =""/></td>
</tr>
<tr><td>
</td><td></td><td><button id="refreshID" type="submit">Status Check</button></td></tr>
<tr><td></td><td></td><td><h2 id="result"></h2></td></tr>
</tbody>
</table>

</body>
</html>