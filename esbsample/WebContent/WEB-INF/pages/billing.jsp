<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<style type="text/css">
table {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	margin: 0px auto;
}

#result {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	color: orange;
}

#canc {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	color: red;
}



.amountDiv {
	text-align: center ;
	padding: 5px;
}

label, input {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	padding: 5px 10px;
}

#payID {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	display: block;
	margin: 0px auto;
	font-weight: bold;
	background: orange;
	color: white;
	height: 40px;
	padding: 5px;
}

#refreshID{
color: white;
background: blue;
padding: 5px 10px;
}

#cancelID{
color: white;
background: blue;
padding: 5px 10px;
}

caption {
	text-align: left;
	color: silver;
	font-weight: bold;
	text-transform: uppercase;
	padding: 5px;
}

thead {
	background: SteelBlue;
	color: white;
}

th, td {
	padding: 5px 10px;
}

tbody tr:nth-child(even) {
	background: WhiteSmoke;
}

tfoot {
	background: SeaGreen;
	color: white;
	text-align: right;
}
</style>
<link href="<c:url value="../css/loading.css" />" rel="stylesheet">
<script  type="text/javascript" src="../js-lib/jquery-1.12.3.js"></script>
<script src="../js/billing.js"></script>
<title>Billing Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div id="resultId" style="display: none" align="center"><h2 id="result"></h2><button id="refreshID" type="submit">Status Check</button><br></div>
<!-- <div id="cancel" style="display: none" align="center"><h2 id="canc"></h2><button id="cancelID" type="submit">Cancel</button><br></div>
 --><table>
  <caption>Billing Items</caption>
  <thead>
    <tr>
      <th>S.No</th>
      <th>Item</th>
      <th>Quantity</th>
      <th>Price</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Burger</td>
      <td>3</td>
      <td>120.00</td>
      <td>260.00</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Pizza</td>
      <td>2</td>
      <td>200.00</td>
      <td>400.00</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Cutlet</td>
      <td>2</td>
      <td>50.00</td>
      <td>100.00</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="4">Grand Total (in Rupees)</th>
      <th>760.00</th>
    </tr>
  </tfoot>
</table>

<table>
<tbody>
<tr>
<td>Enter Tid </td><td>:</td>
<td><input  id="tid" type="text" name="tid"  value = "RTAILWL1"/></td>
</tr>
<tr><td>Enter Amount </td><td>:</td>
<td><input id="amount" type="text" name="amount" class="allownumericwithdecimal" value = "760.00"/></td>
</tr>
<tr><td>Enter Billing Number </td><td>:</td>
<td><input id="billing_number" type="text" name="billing_number" class="allowalphanumeric" value ="1234IRC"/></td></tr>
</tbody>
</table>
<!-- <div class="amountDiv">
<label>Enter TID : </label><td><input id="tid" type="text" name="tid" /></td>
</div> RE0274822M
<div class="amountDiv">
<label>Enter Amount : </label><input id="amount" type="text" name="amount" />
</div>
<div class="amountDiv">
<label>Enter Billing Number : </label><input id="billing_number" type="text" name="billing_number" />
</div> -->
<input id="addntl_attr1" type="hidden" name="addntl_attr1" value="3456"/>
<input id="addntl_atr2" type="hidden" name="addntl_atr2" value="000001"/>
<!-- <input id="billing_number" type="hidden" name="billing_number" value="initiated"/>
 --><button id="payID" type="submit">Proceed To Pay</button>
<!-- <div id="floatingCirclesG" style="display: none">
	<div class="f_circleG" id="frotateG_01"></div>
	<div class="f_circleG" id="frotateG_02"></div>
	<div class="f_circleG" id="frotateG_03"></div>
	<div class="f_circleG" id="frotateG_04"></div>
	<div class="f_circleG" id="frotateG_05"></div>
	<div class="f_circleG" id="frotateG_06"></div>
	<div class="f_circleG" id="frotateG_07"></div>
	<div class="f_circleG" id="frotateG_08"></div>
</div> -->
</body>
</html>