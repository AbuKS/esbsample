<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="../js-lib/jquery-1.12.3.js"></script>
<script src="../js/QueueScrRes.js"></script>
<title>Queue Page</title>

<link rel="stylesheet" href="../css/demo.css">
<link rel="stylesheet" href="../css/form-basic.css">

</head>

<body>

	<div class="main-content">

		<!-- You only need this form and the form-basic.css -->

		<form class="form-basic" >

			<div class="form-row">
				<label> <span>Overall Queue</span> <input type="text"
					name="overallQueue"  id="overallQueue"  value=""/>
				</label>
			</div>

			<div class="form-row">
				<label> <span>Analytica Queue</span> <input type="text"
					name="analyticaQueue" id="analyticaQueue" value=""/>
				</label>
			</div>

			<div class="form-row">
				<label> <span>Crm Queue</span> <input type="text"
					name="crmQueue" id="crmQueue" value=""/>
				</label>
			</div>

			<div class="form-row">
				<label> <span>Erp Queue</span> <input type="text"
					name="erpQueue" id="erpQueue" value=""/>
				</label>
			</div>


			<div class="form-row">
				<button type="submit" id="refreshID">Status Check</button>
			</div>

		</form>

	</div>




</body>
</html>