$(document).ready(function() {
	var interval;
	var count = 0;
	var floatRegex = '[-+]?([0-9]*.[0-9]+|[0-9]+)';
	var regex = '^[a-zA-Z0-9]+$';
	var timeoutvariable;

	function changeBackground(color) {
		document.body.style.background = color;
	}
	
	$('#refreshID').show();

	$('#refreshID').click(function() {

		$.ajax({
			url : "/esbsample/json/countMTQueueNew",

			type : "GET",

			success : function(data) {
				var analyticavalue=data.analyticaQueue;
				$('#analyticaQueue').val(analyticavalue);
				var erpvalue=data.erpQueue;
				$('#erpQueue').val(erpvalue);
				var crmvalue=data.crmQueue;
				$('#crmQueue').val(crmvalue);
				var overallvalue=data.overallQueue;
				$('#overallQueue').val(overallvalue);
			},
			error : function(data) {
				var json = $.parseJSON(data);
				alert(json.error);
			}

		});
	});

});
