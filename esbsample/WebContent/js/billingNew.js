$(document).ready(function() {
	var interval;
	var count=0;
	var floatRegex = '[-+]?([0-9]*.[0-9]+|[0-9]+)'; 
	var regex = '^[a-zA-Z0-9]+$';
    var timeoutvariable;

	$('.allownumericwithdecimal').on('keypress keyup blur',function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
	
	$('.allowalphanumeric').on('keypress keyup blur',function (event) {
		 if ((event.which != 46 || $(this).val().indexOf('.') != 0) && (event.which < 48 || event.which > 57) && (event.which < 65 || event.which > 90)) {
	            event.preventDefault();
	        }
	
	});
	
	$('#payID').click(function() {

		$('#floatingCirclesG').show();
		var amount = $('#amount').val();
		var tid = $('#tid').val();
		var addntl_attr1 = $('#addntl_attr1').val();
		var addntl_atr2 = $('#addntl_atr2').val();
		var billing_number = $('#billing_number').val();
			if( tid == null || amount == null|| tid == '' || amount == '') {
				$('#floatingCirclesG').hide();
				$('#result').html('Tid and Amount fields are mandatory.');
				$('#resultId').show();
				$('#cancel').hide();
				$('#refreshID').hide();							
				$('#result').show();
				$('#canc').hide();

			}else if (tid.length != 8){
				$('#floatingCirclesG').hide();
				$('#result').html('Tid length must be exactly 8 characters.');
				$('#resultId').show();
				$('#cancel').hide();
				$('#refreshID').hide();
				$('#result').show();
				$('#canc').hide();

			}else if (billing_number.length > 20){
				$('#floatingCirclesG').hide();
				$('#result').html('Billing Number minimum 20 characters.');
				$('#resultId').show();
				$('#cancel').hide();
				$('#refreshID').hide();
				$('#result').show();
				$('#canc').hide();

			}else{
					 $.ajax({
							url : "/esbsample/json/initiatePayment",
							data : {
								"amount" : amount,
								"tid" : tid,
								"addntl_attr1" : addntl_attr1,
								"addntl_atr2" : addntl_atr2,
								"billing_number" : billing_number
							},
							type : "GET",
							success : function(response) {
								if (response.message != null && response.message == 'success') {
									$('#floatingCirclesG').hide();
									$('#result').html('Payment initiated Successfully (TID : '+ response.tid+ ', AMOUNT : '+ response.amount+ ', BILL NUMBER : '+ response.billingnumber+ ')');
									$('#resultId').show();
									$('#cancel').show();
									$('#refreshID').hide();
									$('#cancelID').show();
									$('#result').show();
									$('#canc').hide();
									refresh();
								} else {
									$('#floatingCirclesG').hide();
									$('#result').html('Server busy. Please try again later.');
									$('#resultId').show();
									$('#cancel').show();
									$('#refreshID').hide();
									$('#result').show();
									$('#canc').hide();
								}
							},
							error : function(e) {
								$('#floatingCirclesG').hide();
								$('#result').html('Server busy. Please try again later.');
								$('#resultId').show();
								$('#cancel').show();
								$('#refreshID').hide();
								$('#result').show();
								$('#canc').hide();
							}
						});
				}
	
	});
	
	$('#refreshID').click(function() {
		refresh();
	});
	
	function refresh() {
		$('#floatingCirclesG').show();
		var amount = $('#amount').val();
		var tid = $('#tid').val();
		var billingnumber = $('#billing_number').val();
		$.ajax({
			url : "/esbsample/json/statusCheck",
			data : {
				"amount" : amount,
				"tid" : tid,
				"billing_number" : billingnumber
			},
			type : "GET",
			success : function(response) {
				if (response.status != null
						&& response.status == 'payment-successful') {
					$('#floatingCirclesG').hide();
					$('#result').html(
							'Status : Payment Done Successfully (TID : '
									+ response.tid + ', AMOUNT : '
									+ response.amount + ')');
					$('#resultId').show();
					$('#cancel').hide();
					$('#refreshID').hide();
					$('#amount').val('');
					$('#tid').val('');
					$('#billing_number').val('');
					$('#payID').show();
					count=0;
				} else if (response.status != null
						&& response.status == 'payment-pending') {
					count = count + 1;
					if (count > 10) {
						$('#result').html('Transaction timeout');
						$('#floatingCirclesG').hide();
						$('#resultId').show();
						$('#cancel').show();
						$('#refreshID').hide();
						$('#payID').hide();
						$('#cancelID').show();
						count=0;
					} else {
						$('#floatingCirclesG').show();
						$('#result').html(
								'Status : Payment Pending (TID : ' + response.tid
										+ ', AMOUNT : ' + response.amount + ')');
						$('#resultId').show();
						$('#cancel').show();
						$('#refreshID').hide();
						$('#payID').hide();
						$('#cancelID').show();
						timoutVariable = setTimeout(refresh,5000);
						// refresh(count);
					}
				} else if (response.status != null
						&& response.status == 'transaction-declined') {
						$('#floatingCirclesG').hide();
						$('#result').html(
								'Status : Transaction declined (TID : '
										+ response.tid + ', AMOUNT : '
										+ response.amount + ')');
						$('#resultId').show();
						$('#cancel').hide();
						$('#refreshID').hide();
						$('#payID').show();
				} else {
					count = count + 1;
					if (count > 10) {
						$('#result').html('Server busy. Please try after sometime.');
						$('#floatingCirclesG').hide();
						$('#resultId').show();
						$('#cancel').hide();
						$('#refreshID').hide();
						$('#payID').hide();
						count=0;
					} else {
						$('#floatingCirclesG').show();
						$('#result').html('Connecting.. Please wait..');
						$('#resultId').show();
						$('#cancel').hide();
						$('#refreshID').hide();
						$('#payID').hide();
						timoutVariable = setTimeout(refresh, 5000);
						// refresh(count);
					}
				}
			},
			error : function(e) {
				count = count+1;
				if (count > 10) {
					$('#result').html('Transaction timeout.');
					$('#floatingCirclesG').hide();
					$('#resultId').show();
					$('#cancel').hide();
					$('#refreshID').hide();
					$('#payID').hide();
					count=0;
				} else{
				$('#floatingCirclesG').show();
				$('#result').html('Connecting.. Please wait..');
				$('#resultId').show();
				$('#cancel').hide();
				$('#refreshID').hide();
				$('#payID').hide();
				timoutVariable = setTimeout(refresh, 5000);
				}
			}
		});
	}
	
	$('#cancelID').click(function() {
		 clearTimeout(timoutVariable);
		$('#floatingCirclesG').hide();
		var tid = $('#tid').val();	
					 $.ajax({
							url : "/esbsample/json/deletePayment",
							data : {
								"tid" : tid
							},
							type : "POST",
							success : function(event) {
								$('#floatingCirclesG').hide();
								$('#result').hide();
								$('#canc').html('Transaction cancelled');
								$('#resultId').hide();
								$('#canc').show();
								$('#cancel').show();
								$('#refreshID').hide();
								$('#cancelID').hide();
								$('#amount').val('');
								$('#tid').val('');
								$('#billing_number').val('');
								$('#payID').show();
								
							},
							error : function(e) {
								$('#floatingCirclesG').hide();
								$('#result').hide();
								$('#canc').html('Server busy. Please try again later.');
								$('#resultId').hide();
								$('#cancel').show();
								$('#refreshID').hide();
								$('#payID').hide();
							}
						});
					 clearTimeout(timoutVariable);
	});
	
});					


