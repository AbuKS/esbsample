$(document).ready(function() {
	var interval;
	var count = 0;
	var floatRegex = '[-+]?([0-9]*.[0-9]+|[0-9]+)';
	var regex = '^[a-zA-Z0-9]+$';
	var timeoutvariable;

	function changeBackground(color) {
		document.body.style.background = color;
	}

	window.addEventListener("load", function() {
		changeBackground('#e0e0d1');
	});
	$('#refreshID').show();

	$('#refreshID').click(function() {

		$.ajax({
			url : "/esbsample/json/countMTQueue",

			type : "GET",

			success : function(data) {
				var analyticavalue=data.analyticaQueue;
				$('#analyticaQueue').val(analyticavalue);
				var erpvalue=data.erpQueue;
				$('#erpQueue').val(erpvalue);
				var crmvalue=data.crmQueue;
				$('#crmQueue').val(crmvalue);
				var overallvalue=data.overallQueue;
				$('#overallQueue').val(overallvalue);
				var esds1value=data.doneForTheDayESDS1;
				$('#doneForTheDayESDS1').val(esds1value);
				var esds2value=data.doneForTheDayESDS2;
				$('#doneForTheDayESDS2').val(esds2value);
				var doneForERPValue=data.doneForTheDayERP;
				$('#doneForTheDayERP').val(doneForERPValue);
				var finalValue=data.finalDoneForTheDay;
				$('#finalDoneForTheDay').val(finalValue);
				var txnesdsValue=data.txnCountESDS;
				$('#txnCountESDS').val(txnesdsValue);
				var crmCountValue=data.crmCountStatus;
				$('#crmCountStatus').val(crmCountValue);
				var saleESDSValue=data.saleCountESDS;
				$('#saleCountESDS').val(saleESDSValue);
				var updatedtime=data.lastUpdatedTime;
				$('#lastUpdatedTime').val(updatedtime);
				var Note=data.note;
				$('#note').val(Note);
			},
			error : function(data) {
				var json = $.parseJSON(data);
				alert(json.error);
			}

		});
	});

});
