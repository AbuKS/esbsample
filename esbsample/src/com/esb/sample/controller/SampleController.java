package com.esb.sample.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.RetailDetails;
import com.esb.sample.model.RetailResponseDetails;
import com.esb.sample.model.RetailType3Details;
import com.esb.sample.model.RetailType4Details;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.service.SampleService;
import com.esb.sample.service.SampleServiceNew;

@RestController
public class SampleController {

	@Autowired
	private SampleService sampleService;
	

	@RequestMapping(value = "/testSwitchConnection", method = { RequestMethod.GET })
	public String testSwitchConnection() {

		String url = "https://lb.mrlpay.com/tms_server/param/download.php";
		StringBuffer response = new StringBuffer();
		try {
			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, // key manager
					trust_mgr, // trust manager
					new SecureRandom()); // random number generator
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return false;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			URL urlObj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) urlObj.openConnection();

			con.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.toString();
	}

	@RequestMapping(value = "/testURL", method = { RequestMethod.GET })
	public String testURL(HttpServletRequest request) {
		String parameter = request.getParameter("parameter");
		StringBuilder builder = new StringBuilder();
		builder.append("Parameter value : ");
		builder.append(parameter);
		builder.append(" received successfully");
		return builder.toString();
	}

	@RequestMapping(value = "/retail", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public RetailDetails retail(HttpServletRequest request) {
		String tid = request.getParameter("tid");
		RetailDetails retailDetails = sampleService.retail(tid);
		retailDetails.setTid(tid);
		return retailDetails;
	}

	@RequestMapping(value = "/retailresponse", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public RetailResponseDetails retailresponse(HttpServletRequest request) {
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String invoicenum = request.getParameter("invoicenum");
		String stanid = request.getParameter("stanid");
		String response = request.getParameter("response");
		RetailResponseDetails retailDetails = sampleService.updateBillingStatus(tid, amount, invoicenum, stanid);
		return retailDetails;
	}	

	@RequestMapping(value = "/nicBillingPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView nicBillingPage(HttpServletRequest request) {
		return new ModelAndView("billingNew");
	}
	
	@RequestMapping(value = "/fetchDetailType4", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public RetailType4Details fetchDetailType4(HttpServletRequest request) {
		RetailType4Details retailDetails = new RetailType4Details();
		String tid = request.getParameter("tid");
		if (StringUtils.isBlank(tid)) {
			retailDetails.setResponse_code(1);
			retailDetails.setResponse_message("TID field is mandatory.");
			return retailDetails;
		}
		return retailDetails = sampleService.fetchDetailType4(tid);

	}


	@RequestMapping(value = "/retailPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView retailPage(HttpServletRequest request) {
		return new ModelAndView("retailPage");
	}
	
	@RequestMapping(value = "/processBillingItems", method = { RequestMethod.POST })
	@ResponseBody
	public String processBillingItems(HttpServletRequest request) {
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String stan = request.getParameter("stan");
		String invoice = request.getParameter("invoice");
		String status = request.getParameter("status");
		String processStatus = sampleService.processBillingItems(tid, invoice, stan, amount, status);
		if ("success".equalsIgnoreCase(processStatus))
			return "Status : Payment has been initiated";
		else
			return "Status : Server busy. Please try again later";
	}

	@RequestMapping(value = "/initiatePayment", method = { RequestMethod.GET }, headers = {
	"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public PaymentDetails initiatePayment(HttpServletRequest request) {
		PaymentDetails paymentDetails =new PaymentDetails();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String addntl_attr1 = request.getParameter("addntl_attr1");
		String addntl_atr2 = request.getParameter("addntl_atr2");
		String billingnumber = request.getParameter("billing_number");
		String rrn = request.getParameter("rrn");
		if(StringUtils.isBlank(tid.toUpperCase()) || StringUtils.isBlank(amount)){
			paymentDetails.setTid(tid.toUpperCase());
			paymentDetails.setAmount(amount);
			paymentDetails.setMessage("Tid and Amount fields are mandatory.");
			return paymentDetails;
		}else{
			if(billingnumber.isEmpty()){
				 billingnumber = "0";
			}
			paymentDetails = sampleService.initiatePayment(tid.toUpperCase(), amount, addntl_attr1, addntl_atr2,
					billingnumber,rrn);
			return paymentDetails;
		}
	}

	@RequestMapping(value = "/fetchPayment", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public PaymentDetails fetchPayment(HttpServletRequest request) {
		String tid = request.getParameter("tid");
		PaymentDetails retailDetails = sampleService.fetchPayment(tid);
		return retailDetails;
	}

	@RequestMapping(value = "/updateResponse", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public PaymentDetails updateResponse(HttpServletRequest request) {
		PaymentDetails retailDetails = new PaymentDetails();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String response = request.getParameter("response");
		String billingnumber = request.getParameter("billing_number");
		String addntl_attr1 = request.getParameter("addntl_attr1");
		String addntl_atr2 = request.getParameter("addntl_atr2");
		String rrn =request.getParameter("rrn");
		if (StringUtils.isBlank(tid)) {
			retailDetails.setMessage("Invalid TID. Please enter valid TID.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBilling_number(billingnumber);
			return retailDetails;
		} else if (StringUtils.isBlank(amount)) {
			retailDetails.setMessage("Invalid Amount. Please enter valid Amount.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBilling_number(billingnumber);
			return retailDetails;
		} else if (StringUtils.isBlank(billingnumber)) {
			retailDetails.setMessage("Invalid Billingnumber. Please enter valid Billingnumber.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBilling_number(billingnumber);
			return retailDetails;
		} else {
			retailDetails = sampleService.updateResponse(tid.toUpperCase(), amount, response, billingnumber, addntl_attr1,
					addntl_atr2,rrn);
			return retailDetails;
		}
	}

	@RequestMapping(value = "/statusCheck", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public StatusDetails statusCheck(HttpServletRequest request) {
		StatusDetails statusDetails = new StatusDetails();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String billingnumber = request.getParameter("billing_number");
		if(StringUtils.isBlank(tid) || StringUtils.isBlank(amount) || StringUtils.isBlank(billingnumber)){
			statusDetails.setTid(tid.toUpperCase());
			statusDetails.setAmount(amount);
			statusDetails.setBillingnumber(billingnumber);
			statusDetails.setMessage("Tid and Amount and BillingNumber fields are mandatory.");
			return statusDetails;
		} else {
			 statusDetails = sampleService.statusCheck(tid.toUpperCase(), amount, billingnumber);
			if ("success".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("payment-successful");
				statusDetails.setMessage("success");
			} else if ("initiate".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("payment-pending");
				statusDetails.setMessage("success");
			} else if ("failed".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("transaction-declined");
				statusDetails.setMessage("failure");
			}else if ("Data mismatch or Record Notfound".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("Data mismatch or not found. Please check");
				statusDetails.setMessage("failure");
			}
			
			return statusDetails;
		}
	}
	
	@RequestMapping(value = "/deletePayment", method = { RequestMethod.POST }, headers = {
	"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Object deletePayment(HttpServletRequest req) {
		String tid = req.getParameter("tid");
		return sampleService.deletePayment(tid.toUpperCase());
	}
	
	
	@RequestMapping(value = "/sendSMS", method = { RequestMethod.GET }, headers = {
	"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Object sendSMS(HttpServletRequest req) {
		String mobile_number= req.getParameter("mobile_number");
		String content = req.getParameter("content");
		return sampleService.sendSMS(mobile_number, content);
	}

	@RequestMapping(value = "/fetchBillingStatus", method = { RequestMethod.POST })
	@ResponseBody
	public String fetchBillingStatus(HttpServletRequest request) {
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String status = sampleService.fetchBillingStatus(tid, amount);
		if ("success".equalsIgnoreCase(status))
			return "Status : Payment has been processed successfully";
		else
			return "Status : Payment is under process";
	}

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		} };
		return certs;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String uploadFileHandler(@RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			try {
				String fileName = file.getOriginalFilename();
				File[] listOfFiles = new File(
						"C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\esbsample\\").listFiles();
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile() && fileName.equalsIgnoreCase(listOfFiles[i].getName())) {
						return "This file name already exists. Please rename the file and try again";
					}
				}
				String link = "http://182.18.180.27:1234/esbsample/" + fileName;
				String fullFilePath = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\esbsample\\"
						+ fileName;
				File newfile = new File(fullFilePath);
				file.transferTo(newfile);
				return "You have been uploaded the file successfully. For download, please use this link " + link;
			} catch (Exception e) {
				return "File uploading was failed " + e.getMessage();
			}
		} else {
			return "File is empty. Please check";
		}
	}

	/*
	 * public static void main(String args[]) { // creating sample Collection
	 * List<Integer> myList = new ArrayList<Integer>(); for (int i = 0; i < 10;
	 * i++) myList.add(i);
	 * 
	 * myList.forEach(list->{ if(list == 2){ System.out.println(list); } });
	 * 
	 * Map<String, Integer> items = new HashMap<>(); items.put("A", 10);
	 * items.put("B", 20); items.put("C", 30); items.put("D", 40);
	 * items.put("E", 50); items.put("F", 60);
	 * 
	 * items.forEach((k,v)-> { if(k.equalsIgnoreCase("C")){
	 * System.out.println("Key : " + k + " Value : " + v); } }); }
	 */

	
	@RequestMapping(value = "/updateResponseType3", method = { RequestMethod.POST }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public RetailType3Details updateResponseType3(@RequestBody RetailType3Details retailDetails) {
		// this method is for update response

		RetailType3Details retailDetail = new RetailType3Details();
		String tid = retailDetails.getTid();
		String amount = retailDetails.getAmount();
		String billingnumber = retailDetails.getBilling_number();
		String rrn =retailDetails.getRrn();
		String status = retailDetails.getStatus();
		String txn_date = retailDetails.getTxn_date();
		String txn_time = retailDetails.getTxn_time();
		String card_scheme = retailDetails.getCard_scheme();
		String mid = retailDetails.getMid();
		String masked_card_number = retailDetails.getMasked_card_number();
		String txn_type = retailDetails.getTxn_type();				
	
		// if check Urn,tid,amount,billingnumber is not blank
		if( StringUtils.isBlank(txn_type) || "CRDB".equalsIgnoreCase(txn_type)){
		if (StringUtils.isBlank(tid.toUpperCase()) || StringUtils.isBlank(amount) || StringUtils.isBlank(rrn) || StringUtils.isBlank(card_scheme) ||  StringUtils.isBlank(txn_date) || StringUtils.isBlank(txn_time) || StringUtils.isBlank(mid) || StringUtils.isBlank(masked_card_number)) {
			retailDetail.setTid(tid.toUpperCase());
			retailDetail.setAmount(amount);
			retailDetail.setRrn(rrn);
			retailDetail.setCard_scheme(card_scheme);
			retailDetail.setTxn_date(txn_date);
			retailDetail.setTxn_date(txn_time);
			retailDetail.setMid(mid);
			retailDetail.setMasked_card_number(masked_card_number);
			retailDetail.setMessage("Tid,Amount,RRN,Cardscheme,Txndate,Txntime,Mid,Maskedcardnumber fields are mandatory.");
			return retailDetail;
		}else {
			retailDetail = sampleService.updateResponseType3(retailDetails);
			return retailDetail;
		}
		}else{
			if (StringUtils.isBlank(tid.toUpperCase()) || StringUtils.isBlank(amount) || StringUtils.isBlank(rrn) ||  StringUtils.isBlank(txn_date) || StringUtils.isBlank(txn_time) || StringUtils.isBlank(mid)) {
				retailDetail.setTid(tid.toUpperCase());
				retailDetail.setAmount(amount);
				retailDetail.setRrn(rrn);
				retailDetail.setTxn_date(txn_date);
				retailDetail.setTxn_date(txn_date);
				retailDetail.setMid(mid);
				retailDetail.setMessage("Tid,Amount,RRN,Txndate,Txntime,Mid fields are mandatory.");
				return retailDetail;
			}else {
				retailDetail = sampleService.updateResponseType3(retailDetails);
				return retailDetail;
			}
		}
	}
	
	@RequestMapping(value = "/fetchDetailType3", method = { RequestMethod.GET }, headers = {

			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)

	public List<RetailType3Details> fetchDetailType3(HttpServletRequest request) {
		List<RetailType3Details> retaillist = new ArrayList<RetailType3Details>();
		RetailType3Details retailDetails = new RetailType3Details();
		String tid = request.getParameter("tid");
		if (StringUtils.isBlank(tid)) {
			retailDetails.setMessage("TID field is mandatory.");
			retaillist.add(retailDetails);
			return retaillist;
		}
		return retaillist = sampleService.fetchDetailType3(tid);

	}



}
