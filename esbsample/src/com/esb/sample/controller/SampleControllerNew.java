package com.esb.sample.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.PaymentDetailsold;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.model.ReturnObject;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.service.SampleServiceNew;
import com.esb.sample.service.Type2Service;

@RestController
public class SampleControllerNew {

	@Autowired
	private SampleServiceNew sampleServiceNew;
	
	@Autowired
	private Type2Service type2Service;

	@RequestMapping(value = "/showBillingPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView showBillingPage(HttpServletRequest request) {
		return new ModelAndView("billing");
	}
	
	@RequestMapping(value = "/apiCheck", method = { RequestMethod.POST }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public ReturnObject apiCheck(HttpServletRequest request) {
		ReturnObject returnObject = new ReturnObject();
		returnObject.setReturnCode(0);
		returnObject.setReturnMessage("Success");
		return returnObject;
	}

	@RequestMapping(value = "/retailBillingPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView nicBillingPage(HttpServletRequest request) {
		return new ModelAndView("retailType2");
	}
	
	@RequestMapping(value = "/urlPageNew", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView urlPageNew(HttpServletRequest request) {
		return new ModelAndView("QueueScreenNew");
	}
	
	@RequestMapping(value = "/urlPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView urlPage(HttpServletRequest request) {
		return new ModelAndView("QueueScreen");
	}
	
	@RequestMapping(value = "/urlTestPage", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView urlTestPage(HttpServletRequest request) {
		return new ModelAndView("QueueScrRes");
	}
	
	@RequestMapping(value = "/countMTQueueNew", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Object countMTQueueNew() {
		System.out.println("calling count new method....");
		return sampleServiceNew.countMTQueueNew();
	}
	
	@RequestMapping(value = "/countMTQueue", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Object countMTQueue() {
		return sampleServiceNew.countMTQueue();
	}
	
	@RequestMapping(value = "/initiatePaymentNew", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public PaymentDetailsold initiatePaymentNew(HttpServletRequest request) {
		// this method is for initating a payment
		PaymentDetailsold paymentDetails = new PaymentDetailsold();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String addntl_attr1 = request.getParameter("addntl_attr1");
		String addntl_atr2 = request.getParameter("addntl_atr2");
		String billingnumber = request.getParameter("billing_number");
		String rrn = request.getParameter("rrn");
		if (StringUtils.isBlank(tid.toUpperCase()) || StringUtils.isBlank(amount)) {
			paymentDetails.setTid(tid.toUpperCase());
			paymentDetails.setAmount(amount);
			paymentDetails.setMessage("Tid and Amount fields are mandatory.");
			return paymentDetails;
		} else {
			if (billingnumber.isEmpty()) {
				billingnumber = "0";
			}
			// calling service class for initiating new payment
			paymentDetails = sampleServiceNew.showinitiatePaymentNew(tid.toUpperCase(), amount, addntl_attr1, addntl_atr2,
					billingnumber, rrn);
			return paymentDetails;
		}
	}

	@RequestMapping(value = "/fetchPaymentNew", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public PaymentDetailsold fetchPaymentNew(HttpServletRequest request) {
		// this method is for fetch payment
		String tid = request.getParameter("tid");
		PaymentDetailsold retailDetails = sampleServiceNew.showfetchPaymentNew(tid);
		return retailDetails;
	}

	@RequestMapping(value = "/updateResponseNew", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public PaymentDetailsold updateResponseNew(HttpServletRequest request) {
		// this method is for update response
		PaymentDetailsold retailDetails = new PaymentDetailsold();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String response = request.getParameter("response");
		String billingnumber = request.getParameter("billing_number");
		String addntl_attr1 = request.getParameter("addntl_attr1");
		String addntl_atr2 = request.getParameter("addntl_atr2");
		String rrn = request.getParameter("RRN");
		// if check tid,amount,billingnumber is not blank
		if (StringUtils.isBlank(tid)) {
			retailDetails.setMessage("Invalid TID. Please enter valid TID.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBillingnumber(billingnumber);
			return retailDetails;
		} else if (StringUtils.isBlank(amount)) {
			retailDetails.setMessage("Invalid Amount. Please enter valid Amount.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBillingnumber(billingnumber);
			return retailDetails;
		} else if (StringUtils.isBlank(billingnumber)) {
			retailDetails.setMessage("Invalid Billingnumber. Please enter valid Billingnumber.");
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setBillingnumber(billingnumber);
			return retailDetails;
			// calling service class for update response
		} else {
			retailDetails = sampleServiceNew.showupdateResponseNew(tid.toUpperCase(), amount, response, billingnumber,
					addntl_attr1, addntl_atr2, rrn);
			return retailDetails;
		}
	}

	@RequestMapping(value = "/statusCheckNew", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public StatusDetails statusCheckNew(HttpServletRequest request) {
		// th
		StatusDetails statusDetails = new StatusDetails();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		String billingnumber = request.getParameter("billing_number");
		// if check tid,amount,billingnumber is not blank
		if (StringUtils.isBlank(tid) || StringUtils.isBlank(amount) || StringUtils.isBlank(billingnumber)) {
			statusDetails.setTid(tid.toUpperCase());
			statusDetails.setAmount(amount);
			statusDetails.setBillingnumber(billingnumber);
			statusDetails.setMessage("Tid and Amount and BillingNumber fields are mandatory.");
			return statusDetails;
			// calling service class for status method
		} else {
			statusDetails = sampleServiceNew.showstatusCheckNew(tid.toUpperCase(), amount, billingnumber);
			// if check status is success or initiate or failed
			if ("success".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("payment-successful");
				statusDetails.setMessage("success");
			} else if ("initiate".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("payment-pending");
				statusDetails.setMessage("success");
			} else if ("failed".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("transaction-declined");
				statusDetails.setMessage("failure");
			} else if ("Data mismatch or Record Notfound".equalsIgnoreCase(statusDetails.getStatus())) {
				statusDetails.setStatus("Data mismatch or not found. Please check");
				statusDetails.setMessage("failure");
			}

			return statusDetails;
		}
	}

	@RequestMapping(value = "/deletePaymentNew", method = { RequestMethod.POST }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Object deletePaymentNew(HttpServletRequest req) {
		String tid = req.getParameter("tid");
		return sampleServiceNew.showdeletePaymentNew(tid.toUpperCase());
	}

	@RequestMapping(value = "/fetchDetails", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public RetailType2Details retailType2response(HttpServletRequest request) {
		RetailType2Details retailDetails = new RetailType2Details();
		String tid = request.getParameter("tid");
		String amount = request.getParameter("amount");
		
		
		if (StringUtils.isBlank(tid.toUpperCase()) || StringUtils.isBlank(amount)) {
			retailDetails.setTid(tid.toUpperCase());
			retailDetails.setAmount(amount);
			retailDetails.setMessage("Tid and Amount fields are mandatory.");
			return retailDetails;
		}
		return retailDetails = type2Service.retailType2response(tid, amount);
	}
	
}
