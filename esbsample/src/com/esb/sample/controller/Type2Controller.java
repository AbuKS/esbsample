package com.esb.sample.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.esb.sample.model.InsertReqDetails;
import com.esb.sample.model.InsertResponseDetails;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.service.SampleServiceNew;
import com.esb.sample.service.Type2Service;

@RestController
public class Type2Controller {
	@Autowired
	private Type2Service type2Service;

	@Autowired
	private SampleServiceNew sampleServiceNew;

	@RequestMapping(value = "/initiateDetailType2", method = { RequestMethod.POST }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public InsertResponseDetails initiatePaymentType2(@RequestBody InsertReqDetails retailDetail) {
		// this method is for initating a payment
		InsertResponseDetails paymentDetails = new InsertResponseDetails();
		String tid = retailDetail.getTid();
		String amount = retailDetail.getAmount();
		if (StringUtils.isBlank(amount)) {
			paymentDetails.setResponse_code(1);
			paymentDetails.setResponse_message("Amount fields are mandatory.");
			paymentDetails.setTid(tid.toUpperCase());
			paymentDetails.setAmount(amount);
			return paymentDetails;
		} else {

			// calling service class for initiating new payment
			paymentDetails = sampleServiceNew.initiatePaymentType2(retailDetail);
			return paymentDetails;
		}
	}

	@RequestMapping(value = "/fetchDetailType2", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public ResDetailsForRetailType fetchDetailType2(HttpServletRequest request) {
		ResDetailsForRetailType retailDetails = new ResDetailsForRetailType();
		String urn = request.getParameter("urn");
		if (StringUtils.isBlank(urn)) {
			retailDetails.setResponse_code(1);
			retailDetails.setResponse_message("URN field is mandatory.");
			return retailDetails;
		}
		return retailDetails = type2Service.fetchUrnDetails(urn);
	}

	@RequestMapping(value = "/updateDetailType2", method = { RequestMethod.POST }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResDetailsForRetailType updateDetailType2(@RequestBody RetailType2Details retailDetails) {
		ResDetailsForRetailType retailDetail = new ResDetailsForRetailType();
		String tid = retailDetails.getTid();
		String urn = retailDetails.getUrn();
		String amount = retailDetails.getAmount();
		String rrn = retailDetails.getRrn();
		String card_scheme = retailDetails.getCard_scheme();
		String txn_date = retailDetails.getTxn_date();
		String txn_time = retailDetails.getTxn_time();
		String mid = retailDetails.getMid();
		String masked_card_number = retailDetails.getMasked_card_number();
		String txn_type = retailDetails.getTxn_type();
		retailDetail.setTid(tid.toUpperCase());
		retailDetail.setAmount(amount);
		retailDetail.setUrn(urn);
		;
		// if check Urn,tid,amount,billingnumber is not blank
		if (StringUtils.isBlank(urn)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid URN. Please enter valid URN.");
			return retailDetail;
		} else if (StringUtils.isBlank(amount)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid Amount. Please enter valid Amount.");
			return retailDetail;
		} else if (StringUtils.isBlank(rrn)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid RRN. Please enter valid RRN.");
			return retailDetail;
		} else if (StringUtils.isBlank(txn_date)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid Txn_Date. Please enter valid Txn_Date.");
			return retailDetail;
		} else if (StringUtils.isBlank(txn_time)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid Txn_Time. Please enter valid Txn_Time.");
			return retailDetail;
		} else if (StringUtils.isBlank(mid)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid Mid. Please enter valid Mid.");
			return retailDetail;
		} else if (StringUtils.isBlank(txn_type)) {
			retailDetail.setResponse_code(1);
			retailDetail.setResponse_message("Invalid Txn_Type. Please enter valid Txn_Type.");
			return retailDetail;
		} else if ("CRDB".equals(txn_type)) {

			if (StringUtils.isBlank(card_scheme)) {
				retailDetail.setResponse_code(1);
				retailDetail.setResponse_message("Invalid Card_Scheme. Please enter valid Card_Scheme.");
				return retailDetail;
			} else if (StringUtils.isBlank(masked_card_number)) {
				retailDetail.setResponse_code(1);
				retailDetail.setResponse_message("Invalid Masked_Card_Number. Please enter valid Masked_Card_Number.");
				return retailDetail;
			}
		}

		retailDetail = sampleServiceNew.updateResponseType2(retailDetails);
		return retailDetail;

	}

}
