package com.esb.sample.service.impl;

import java.util.ArrayList;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;

import com.esb.sample.dao.SampleDaoNew;
import com.esb.sample.model.InsertReqDetails;
import com.esb.sample.model.InsertResponseDetails;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.PaymentDetailsold;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.service.SampleServiceNew;
import com.esb.sample.util.Utils;

@ManagedBean
public class SampleServiceImplNew implements SampleServiceNew {

	@Autowired
	private SampleDaoNew sampleDaoNew;

	@Override
	public InsertResponseDetails initiatePaymentType1(InsertReqDetails retailDetail) {
		return sampleDaoNew.initiatePaymentNew(retailDetail);
	}

	@Override
	public InsertResponseDetails initiatePaymentType2(InsertReqDetails retailDetail) {
		return sampleDaoNew.initiatePaymentType2(retailDetail);
	}

	@Override
	public ArrayList<ResDetailsForRetailType> fetchPaymentNew(String tid) {
		return sampleDaoNew.fetchPaymentNew(tid);
	}

	@Override
	public ResDetailsForRetailType updateResponseNew(RetailType2Details retailDetails) {
		return sampleDaoNew.updateResponseNew(retailDetails);
	}

	@Override
	public ResDetailsForRetailType updateResponseType2(RetailType2Details retailDetails) {
		return sampleDaoNew.updateResponseType2(retailDetails);
	}

	@Override
	public PaymentDetailsold showinitiatePaymentNew(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber, String rrn) {
		return sampleDaoNew.showinitiatePaymentNew(tid, amount, addntl_attr1, addntl_atr2, billingnumber, rrn);
	}

	@Override
	public PaymentDetailsold showfetchPaymentNew(String tid) {
		return sampleDaoNew.showfetchPaymentNew(tid);
	}

	@Override
	public PaymentDetailsold showupdateResponseNew(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2, String rrn) {
		return sampleDaoNew.showupdateResponseNew(tid, amount, response, billingnumber, addntl_attr1, addntl_atr2, rrn);
	}

	@Override
	public StatusDetails showstatusCheckNew(String tid, String amount, String billingnumber) {
		return sampleDaoNew.showstatusCheckNew(tid, amount, billingnumber);
	}

	@Override
	public Object showdeletePaymentNew(String tid) {
		return sampleDaoNew.showdeletePaymentNew(tid);
	}

	@Override
	public Object countMTQueue() {
		Object jsonObject = null;
		String url = "http://192.168.153.183:8080/esb/json/countMTQueue";

		try {

			jsonObject = new Utils().populateHttpOrHttpsJsonObject(null, url, "GET", null);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	@Override
	public Object countMTQueueNew() {
		Object jsonObject = null;
		String url = "http://192.168.153.183:8080/esb/json/countMTQueueNew";
		try {
			jsonObject = new Utils().populateHttpOrHttpsJsonObject(null, url, "GET", null);
			System.out.println("Response :" + jsonObject.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	/*
	 * @Override public ReturnObject blockingApi(String id, String model, String
	 * dealId, String serialNumber, String schemeId, String name, String
	 * imeiNumber, String flag) { return sampleDaoNew.blockingApi(id, model,
	 * dealId, serialNumber, schemeId, name, imeiNumber, flag); }
	 * 
	 * @Override public ReturnObject unblockingApi(String id, String flag) {
	 * return sampleDaoNew.unblockingApi(id,flag); }
	 */

}
