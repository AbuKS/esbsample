package com.esb.sample.service.impl;

import java.util.List;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;

import com.esb.sample.dao.SampleDao;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.RetailDetails;
import com.esb.sample.model.RetailResponseDetails;
import com.esb.sample.model.RetailType3Details;
import com.esb.sample.model.RetailType4Details;
import com.esb.sample.model.ReturnObject;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.service.SampleService;

@ManagedBean
public class SampleServiceImpl implements SampleService {

	@Autowired
	private SampleDao sampleDao;

	@Override
	public RetailDetails retail(String tid) {
		return sampleDao.retail(tid);
	}

	@Override
	public String processBillingItems(String tid, String invoice, String stan, String amount, String status) {
		return sampleDao.processBillingItems(tid, invoice, stan, amount, status);
	}

	@Override
	public RetailResponseDetails updateBillingStatus(String tid, String amount, String invoicenum, String stanid) {
		return sampleDao.updateBillingStatus(tid, amount, invoicenum, stanid);
	}

	@Override
	public String fetchBillingStatus(String tid, String amount) {
		return sampleDao.fetchBillingStatus(tid, amount);
	}

	@Override
	public PaymentDetails initiatePayment(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber,String rrn) {
		return sampleDao.initiatePayment(tid, amount, addntl_attr1, addntl_atr2, billingnumber,rrn);

	}

	@Override
	public PaymentDetails fetchPayment(String tid) {
		return sampleDao.fetchPayment(tid);
	}

	@Override
	public PaymentDetails updateResponse(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2,String rrn) {
		return sampleDao.updateResponse(tid, amount, response, billingnumber, addntl_attr1, addntl_atr2,rrn);

	}

	@Override
	public StatusDetails statusCheck(String tid, String amount, String billingnumber) {
		return sampleDao.statusCheck(tid, amount, billingnumber);

	}

	@Override
	public Object deletePayment(String tid) {
		return sampleDao.deletePayment(tid);
	}

	@Override
	public RetailType3Details updateResponseType3(RetailType3Details retailDetails) {
		return sampleDao.updateResponseType3(retailDetails);
	}

	@Override
	public  List<RetailType3Details> fetchDetailType3(String tid) {
		return sampleDao.fetchDetailType3(tid);
	}

	@Override
	public ReturnObject sendSMS(String mobile_number, String content) {
		return sampleDao.sendSMS(mobile_number, content);
	}

	@Override
	public RetailType4Details fetchDetailType4(String tid) {
		return sampleDao.fetchDetailType4(tid);
	}

}
