package com.esb.sample.service.impl;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;

import com.esb.sample.dao.Type2Dao;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.service.Type2Service;

@ManagedBean
public class Type2ServiceImpl implements Type2Service {

	@Autowired
	private Type2Dao type2Dao;

	@Override
	public RetailType2Details retailType2response(String tid,String amount) {
		return type2Dao.retailType2response(tid,amount);
	}
	
	@Override
	public RetailType2Details addUrnDetails(String tid,String amount) {
		return type2Dao.addUrnDetail(tid,amount);
	}
	
	@Override
	public ResDetailsForRetailType fetchUrnDetails(String urn) {
		return type2Dao.fetchUrnDetail(urn);
	}
	
	

}
