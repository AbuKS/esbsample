package com.esb.sample.service;

import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;

public interface Type2Service {

	public RetailType2Details retailType2response(String tid,String amount);
	public RetailType2Details addUrnDetails(String tid,String amount);
	public ResDetailsForRetailType fetchUrnDetails(String urn);
}
