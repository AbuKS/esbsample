package com.esb.sample.service;

import java.util.List;

import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.RetailDetails;
import com.esb.sample.model.RetailResponseDetails;
import com.esb.sample.model.RetailType3Details;
import com.esb.sample.model.RetailType4Details;
import com.esb.sample.model.ReturnObject;
import com.esb.sample.model.StatusDetails;

public interface SampleService {

	public RetailDetails retail(String tid);

	public String processBillingItems(String tid, String invoice, String stan, String amount, String status);

	public RetailResponseDetails updateBillingStatus(String tid, String amount, String invoicenum, String stanid);

	public String fetchBillingStatus(String tid, String amount);

	public PaymentDetails initiatePayment(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber,String rrn);

	public PaymentDetails fetchPayment(String tid);

	public PaymentDetails updateResponse(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2, String rrn);

	public StatusDetails statusCheck(String tid, String amount, String billingnumber);

	public Object deletePayment(String tid);

	public RetailType3Details updateResponseType3(RetailType3Details retailDetails);

	public  List<RetailType3Details> fetchDetailType3(String tid);

	public ReturnObject sendSMS(String mobile_number, String content);

	public RetailType4Details fetchDetailType4(String tid);

}
