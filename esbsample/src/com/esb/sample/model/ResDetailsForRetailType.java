package com.esb.sample.model;

public class ResDetailsForRetailType {

	private String tid;
	private String amount;
	private String billing_number;
	private int response_code;
	private String response_message;
	private String additional_attribute1;
	private String additional_attribute2;
	private String additional_attribute3;
	private String rrn;
	private String card_scheme;
	private String txn_date;
	private String txn_time;
	private String mid;
	private String masked_card_number;
	private String txn_type;
	private String status;
	private String urn;
	private String organization_code;

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBilling_number() {
		return billing_number;
	}

	public void setBilling_number(String billing_number) {
		this.billing_number = billing_number;
	}

	public int getResponse_code() {
		return response_code;
	}

	public void setResponse_code(int response_code) {
		this.response_code = response_code;
	}

	public String getResponse_message() {
		return response_message;
	}

	public void setResponse_message(String response_message) {
		this.response_message = response_message;
	}

	public String getAdditional_attribute1() {
		return additional_attribute1;
	}

	public void setAdditional_attribute1(String additional_attribute1) {
		this.additional_attribute1 = additional_attribute1;
	}

	public String getAdditional_attribute2() {
		return additional_attribute2;
	}

	public void setAdditional_attribute2(String additional_attribute2) {
		this.additional_attribute2 = additional_attribute2;
	}

	public String getAdditional_attribute3() {
		return additional_attribute3;
	}

	public void setAdditional_attribute3(String additional_attribute3) {
		this.additional_attribute3 = additional_attribute3;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getCard_scheme() {
		return card_scheme;
	}

	public void setCard_scheme(String card_scheme) {
		this.card_scheme = card_scheme;
	}

	public String getTxn_date() {
		return txn_date;
	}

	public void setTxn_date(String txn_date) {
		this.txn_date = txn_date;
	}

	public String getTxn_time() {
		return txn_time;
	}

	public void setTxn_time(String txn_time) {
		this.txn_time = txn_time;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMasked_card_number() {
		return masked_card_number;
	}

	public void setMasked_card_number(String masked_card_number) {
		this.masked_card_number = masked_card_number;
	}

	public String getTxn_type() {
		return txn_type;
	}

	public void setTxn_type(String txn_type) {
		this.txn_type = txn_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getOrganization_code() {
		return organization_code;
	}

	public void setOrganization_code(String organization_code) {
		this.organization_code = organization_code;
	}

}
