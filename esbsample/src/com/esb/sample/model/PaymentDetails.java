package com.esb.sample.model;

public class PaymentDetails {

	private String tid;
	private String amount;
	private String billing_number;
	private String message;
	private String addntl_attr1;
	private String addntl_attr2;
	private String rrn;
	private String urn;
	
	

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAddntl_attr1() {
		return addntl_attr1;
	}

	public void setAddntl_attr1(String addntl_attr1) {
		this.addntl_attr1 = addntl_attr1;
	}

	

	public String getBilling_number() {
		return billing_number;
	}

	public void setBilling_number(String billing_number) {
		this.billing_number = billing_number;
	}

	public String getAddntl_attr2() {
		return addntl_attr2;
	}

	public void setAddntl_attr2(String addntl_attr2) {
		this.addntl_attr2 = addntl_attr2;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}
		
}
