package com.esb.sample.model;

public class StatusDetails {

	private String tid;
	private String amount;
	private String status;
	private String message;
	private String rrn;
	private String billingnumber;
	
		

	public String getBillingnumber() {
		return billingnumber;
	}

	public void setBillingnumber(String billingnumber) {
		this.billingnumber = billingnumber;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
