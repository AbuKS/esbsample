package com.esb.sample.model;

public class RetailResponseDetails {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
