package com.esb.sample.model;

public class PaymentDetailsold {

	private String tid;
	private String amount;
	private String billingnumber;
	private String message;
	private String addntl_attr1;
	private String addntl_atr2;
	private String rrn;
	
	

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillingnumber() {
		return billingnumber;
	}

	public void setBillingnumber(String billingnumber) {
		this.billingnumber = billingnumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAddntl_attr1() {
		return addntl_attr1;
	}

	public void setAddntl_attr1(String addntl_attr1) {
		this.addntl_attr1 = addntl_attr1;
	}

	public String getAddntl_atr2() {
		return addntl_atr2;
	}

	public void setAddntl_atr2(String addntl_atr2) {
		this.addntl_atr2 = addntl_atr2;
	}

	
}
