package com.esb.sample.model;

public class InsertReqDetails {

	private String tid;
	private String amount;
	private String organization_code;
	private String billing_number;
	private String additional_attribute1;
	private String additional_attribute2;
	private String additional_attribute3;

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOrganization_code() {
		return organization_code;
	}

	public void setOrganization_code(String organization_code) {
		this.organization_code = organization_code;
	}

	public String getBilling_number() {
		return billing_number;
	}

	public void setBilling_number(String billing_number) {
		this.billing_number = billing_number;
	}

	public String getAdditional_attribute1() {
		return additional_attribute1;
	}

	public void setAdditional_attribute1(String additional_attribute1) {
		this.additional_attribute1 = additional_attribute1;
	}

	public String getAdditional_attribute2() {
		return additional_attribute2;
	}

	public void setAdditional_attribute2(String additional_attribute2) {
		this.additional_attribute2 = additional_attribute2;
	}

	public String getAdditional_attribute3() {
		return additional_attribute3;
	}

	public void setAdditional_attribute3(String additional_attribute3) {
		this.additional_attribute3 = additional_attribute3;
	}

}
