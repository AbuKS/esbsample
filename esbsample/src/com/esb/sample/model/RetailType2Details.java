package com.esb.sample.model;

public class RetailType2Details {

	private String tid;
	private String amount;
	private String urn;
	private String message;
	private String status;
	private String billing_number;
	private String rrn;
	private String card_scheme;
	private String txn_date;
	private String txn_time;
	private String mid;
	private String masked_card_number;
	private String txn_type;
	private String addntl_attr1;
	private String addntl_attr2;
	private String addntl_attr3;
	
	

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBilling_number() {
		return billing_number;
	}

	public void setBilling_number(String billing_number) {
		this.billing_number = billing_number;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getCard_scheme() {
		return card_scheme;
	}

	public void setCard_scheme(String card_scheme) {
		this.card_scheme = card_scheme;
	}

	public String getTxn_date() {
		return txn_date;
	}

	public void setTxn_date(String txn_date) {
		this.txn_date = txn_date;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMasked_card_number() {
		return masked_card_number;
	}

	public void setMasked_card_number(String masked_card_number) {
		this.masked_card_number = masked_card_number;
	}

	public String getTxn_time() {
		return txn_time;
	}

	public void setTxn_time(String txn_time) {
		this.txn_time = txn_time;
	}

	public String getTxn_type() {
		return txn_type;
	}

	public void setTxn_type(String txn_type) {
		this.txn_type = txn_type;
	}

	public String getAddntl_attr1() {
		return addntl_attr1;
	}

	public void setAddntl_attr1(String addntl_attr1) {
		this.addntl_attr1 = addntl_attr1;
	}

	public String getAddntl_attr2() {
		return addntl_attr2;
	}

	public void setAddntl_attr2(String addntl_attr2) {
		this.addntl_attr2 = addntl_attr2;
	}

	public String getAddntl_attr3() {
		return addntl_attr3;
	}

	public void setAddntl_attr3(String addntl_attr3) {
		this.addntl_attr3 = addntl_attr3;
	}

	
	

}
