package com.esb.sample.model;

import java.util.List;

public class ReturnObject {

	private int returnCode = 1;
	private String returnMessage = "failure";
	private List resultList;
	private Object object;
	private Object objectNew;

	public Object getObjectNew() {
		return objectNew;
	}

	public void setObjectNew(Object objectNew) {
		this.objectNew = objectNew;
	}

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List getResultList() {
		return resultList;
	}

	public void setResultList(List resultList) {
		this.resultList = resultList;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
