package com.esb.sample.init;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionProd {

	private Connection connection;

	public Connection getConnection(int inFlag) 
	{
		try {
			
			if (inFlag == 1) {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				connection = DriverManager.getConnection(
						"jdbc:sqlserver://192.168.1.65:15333;user=Merchantapp;password=Merapp#@;database=MRLPOSNET_MSCRM");
			} else if (inFlag == 2) {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				connection = DriverManager.getConnection(
						"jdbc:sqlserver://192.168.1.55;user=Merchantapp;password=Merapp#@;database=WEB_CRM");
			} else if (inFlag == 3) {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://10.10.45.100:3306/lms100", "mobileapp", "mobileapp2");
			} else if (inFlag == 4) {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/tx", "root", "developer");
//				127.0.0.1
			}

			return connection;
		} catch (Exception e) {
			e.getMessage();
		} finally {

		}
		return null;

	}

	public void closeConnection(Connection connection) {
		try {
			if (connection != null && !connection.isClosed())
				connection.close();
		} catch (SQLException e) {
			e.getMessage();
		} finally {

		}
	}
}
