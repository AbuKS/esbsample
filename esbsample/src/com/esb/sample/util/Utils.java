package com.esb.sample.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.DecimalFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.esb.sample.model.ReturnObject;

public class Utils {
	
	static final Logger logger = Logger.getLogger(Utils.class);
	static final Logger smsLog = Logger.getLogger("smsLog");
	
	public String getDecimal(String amount){

		double aDouble = Double.parseDouble(amount);   
		DecimalFormat df = new DecimalFormat(".00");
		df.setMaximumFractionDigits(2);
		String decimalAmount = df.format(aDouble);
 		return decimalAmount;	
	}
	
	public Object populateHttpOrHttpsJsonObject(JSONObject requestObject, String url, String methodType,
			JSONArray requestArray) {

		JSONObject responseObject = new JSONObject();

		if (url.contains("https")) {
			HttpsURLConnection con = null;

			try {
				SSLContext ssl_ctx = SSLContext.getInstance("TLS");
				TrustManager[] trust_mgr = get_trust_mgr();
				ssl_ctx.init(null, // key manager
						trust_mgr, // trust manager
						new SecureRandom()); // random number generator
				HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

				HostnameVerifier allHostsValid = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return false;
					}
				};

				// Install the all-trusting host verifier
				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

				URL urlObj = new URL(url);
				con = (HttpsURLConnection) urlObj.openConnection();
				con.setRequestMethod(methodType);
				if ("POST".equalsIgnoreCase(methodType)) {
					con.setDoOutput(true);
					con.setDoInput(true);
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
					BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
					if (null != requestObject)
						wr.write(requestObject.toString());
					else if (null != requestArray)
						wr.write(requestArray.toString());
					wr.flush();
				}
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				String responseString = response.toString();
				logger.error("[Utils]:" + url + ";Response :" + responseString);
				if (StringUtils.isNotBlank(responseString)) {
					responseObject = new JSONObject(responseString);
				}
			} catch (Exception e) {
				logger.error("[Utils][getHttpsConnection] method :  URL : " + url, e);
			}
		} else {
			try {
				URL urlObj = new URL(url);
				HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
				con.setRequestMethod(methodType);
				if ("POST".equalsIgnoreCase(methodType)) {
					con.setDoOutput(true);
					con.setDoInput(true);
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
					BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
					if (null != requestObject)
						wr.write(requestObject.toString());
					else if (null != requestArray)
						wr.write(requestArray.toString());
					wr.flush();
				}
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				String responseString = response.toString();
				logger.error("[Utils]:" + url + ";Response :" + responseString);
				if (StringUtils.isNotBlank(responseString)) {
					responseObject = new JSONObject(responseString);
				}
			} catch (Exception e) {
				logger.error("[Utils][getHttpConnection] method :  URL : " + url, e);
			}
		}
		return responseObject;
    }
	
	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		} };
		return certs;
	}
	
	public static StringBuffer sendSMSNew(String mobileNumber, String content) {
		StringBuffer response = new StringBuffer();
		//StringBuffer response = null;
		try {
			String url = "http://193.105.74.58/api/v3/sendsms/plain?user=MRLPOS&password=MRL@8pos&sender=MRLPOS&SMSText="
					+ URLEncoder.encode(content,java.nio.charset.StandardCharsets.UTF_8.toString()) + "&GSM=91" + mobileNumber + "&type=longSMS";
			smsLog.info(url);
			URL urlObj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();

			con.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			smsLog.info(mobileNumber + "~" + response.toString());
			return response;
		} catch (Exception e) {
			logger.error("Error getting in live transactions notification: mobileNumber=" + mobileNumber, e);
		}
		return response;
		
	}
	
	public static void main(String args[]) {
		/*
		 * StringBuffer res =new Utils().sendSMSNew("7397286096",
		 * "Hiii Testing ...");
		 * 
		 * System.out.println( "Res :"+res.toString());
		 */

		/*ReturnObject res = new Utils().sendSMS("7397286096", "Testing");
		System.out.println("Res :" + res.toString());*/

	}

	public ReturnObject sendSMS(String mobile_number, String text) {
		ReturnObject returnObject = new ReturnObject();
		Object jsonObject = null;
		smsLog.info("Mbl :" + mobile_number);
		smsLog.info("text :" + text);
		String url = "http://182.18.180.27:8080/esbsample/json/sendSMS?mobile_number=" + mobile_number + "&content="
				+ text;
		try {
			smsLog.info("Url :" + url);
			jsonObject = new Utils().populateHttpOrHttpsJsonObject(null, url, "GET", null);
			smsLog.info("Response :" + jsonObject.toString());
			returnObject.setReturnCode(0);
			returnObject.setReturnMessage("Success");
			returnObject.setObject(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnObject;
	}
	
}