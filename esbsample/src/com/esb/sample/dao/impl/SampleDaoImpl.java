package com.esb.sample.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.esb.sample.dao.SampleDao;
import com.esb.sample.init.DBConnectionProd;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.RetailDetails;
import com.esb.sample.model.RetailResponseDetails;
import com.esb.sample.model.RetailType3Details;
import com.esb.sample.model.RetailType4Details;
import com.esb.sample.model.ReturnObject;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.util.Utils;

@ManagedBean
public class SampleDaoImpl implements SampleDao {

	final static Logger logger = Logger.getLogger(SampleDaoImpl.class);

	@Override
	public RetailDetails retail(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		RetailDetails details = new RetailDetails();
		boolean isAmountAvailable = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall("SELECT AMOUNT FROM RETAIL_TABLE WHERE TID=? ORDER BY INSERT_TIMESTAMP DESC");

			pstmt.setString(1, tid);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				isAmountAvailable = true;
				details.setMessage("success");
				details.setAmount(resultSet.getString("AMOUNT"));
				break;
			}
			if (!isAmountAvailable)
				details.setMessage("failure. amount not available.");
		} catch (Exception e) {
			logger.error("retail method TID=" + tid, e);
			details.setMessage("failure. please try again later.");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("retail method TID=" + tid, e);
				details.setMessage("failure. please try again later.");
			}
		}
		return details;
	}

	@Override
	public String processBillingItems(String tid, String invoice, String stan, String amount, String status) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		String processStatus = "failure";
		try {
			conn = new DBConnectionProd().getConnection(4);
			insertStmt = conn.prepareStatement(
					"INSERT INTO RETAIL_TABLE (TID, INVOICE, STAN, AMOUNT, STATUS) VALUES (?, ?, ?, ?, ?);");

			insertStmt.setString(1, tid);
			insertStmt.setString(2, invoice);
			insertStmt.setString(3, stan);
			insertStmt.setString(4, amount);
			insertStmt.setString(5, status);
			insertStmt.executeUpdate();
			processStatus = "success";
		} catch (Exception e) {
			logger.error("Error in processBillingItems method", e);
		} finally {
			try {
				if (insertStmt != null)
					insertStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("Error in processBillingItems method", e);
			}

		}
		return processStatus;
	}

	@Override
	public RetailResponseDetails updateBillingStatus(String tid, String amount, String invoicenum, String stanid) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		RetailResponseDetails responseDetails = new RetailResponseDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE SET STATUS =? WHERE TID=? AND AMOUNT=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, "success");
			updateStmt.setString(2, tid);
			updateStmt.setString(3, amount);
			if (updateStmt.executeUpdate() == 1)
				responseDetails.setMessage("success");
			else
				responseDetails.setMessage("failure. please try again later.");
		} catch (Exception e) {
			logger.error("Error in updateBillingStatus method", e);
			responseDetails.setMessage("failure. please try again later.");
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return responseDetails;
	}

	@Override
	public String fetchBillingStatus(String tid, String amount) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		String status = "";
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT STATUS FROM RETAIL_TABLE WHERE TID=? AND AMOUNT=? ORDER BY INSERT_TIMESTAMP DESC");

			pstmt.setString(1, tid);
			pstmt.setString(2, amount);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				status = resultSet.getString("STATUS");
				break;
			}
		} catch (Exception e) {
			logger.error("fetchBillingStatus method TID=" + tid, e);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchBillingStatus method TID=" + tid, e);
			}
		}
		return status;
	}

	@Override
	public PaymentDetails initiatePayment(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber,String rrn) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		PaymentDetails paymentDetails = new PaymentDetails();
		boolean isRecordAvail = new SampleDaoImpl().checkIsRecordAlreadyAvailable(tid,billingnumber,amount);
		if (!isRecordAvail) {
			try {
				conn = new DBConnectionProd().getConnection(4);
				insertStmt = conn.prepareStatement(
						"INSERT INTO RETAIL_TABLE (TID, AMOUNT, BILLING_NUMBER, ADDNTL_ATTR1, ADDNTL_ATR2,RRN) VALUES (?, ?, ?, ?, ?, ?);");

				insertStmt.setString(1, tid);
				insertStmt.setString(2, new Utils().getDecimal(amount));
				insertStmt.setString(3, billingnumber);
				insertStmt.setString(4, addntl_attr1);
				insertStmt.setString(5, addntl_atr2);
				insertStmt.setString(6, "0");
				insertStmt.executeUpdate();
				paymentDetails.setMessage("success");
				paymentDetails.setTid(tid);
				paymentDetails.setAmount(amount);
				paymentDetails.setBilling_number(billingnumber);

			} catch (Exception e) {
				logger.error("Error in initiatePayment method", e);
			} finally {
				try {
					if (insertStmt != null)
						insertStmt.close();
					new DBConnectionProd().closeConnection(conn);
				} catch (SQLException e) {
					logger.error("Error in initiatePayment method", e);
				}
			}
		} else {
			boolean isUpdate = new SampleDaoImpl().updateStatus(tid, amount, "initiate", billingnumber, addntl_attr1,
					addntl_atr2);

			if (isUpdate) {
				paymentDetails.setMessage("success");
				paymentDetails.setTid(tid);
				paymentDetails.setAmount(amount);
				paymentDetails.setBilling_number(billingnumber);
			}
		}
		return paymentDetails;
	}

	@Override
	public boolean updateStatus(String tid, String amount, String response, String billingnumber, String addntl_attr1,
			String addntl_atr2) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PaymentDetails paymentDetails = new PaymentDetails();
		boolean isUpdate = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE SET STATUS=?,ADDNTL_ATTR1=?,ADDNTL_ATR2=? WHERE TID=? AND BILLING_NUMBER=? AND AMOUNT=?  ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, response);
			updateStmt.setString(2, addntl_attr1);
			updateStmt.setString(3, addntl_atr2);
			updateStmt.setString(4, tid);
			updateStmt.setString(5, billingnumber);
			updateStmt.setString(6, amount);
			updateStmt.executeUpdate();
			isUpdate = true;

		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			paymentDetails.setMessage("failure. please try again later.");
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return isUpdate;
	}

	@Override
	public PaymentDetails fetchPayment(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		PaymentDetails details = new PaymentDetails();
		boolean isAmountAvailable = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT AMOUNT,BILLING_NUMBER FROM RETAIL_TABLE WHERE TID=? AND STATUS='initiate' ORDER BY INSERT_TIMESTAMP DESC");

			pstmt.setString(1, tid.toUpperCase());
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				isAmountAvailable = true;
				details.setMessage("success");
				details.setAmount(resultSet.getString("AMOUNT"));
				details.setBilling_number(resultSet.getString("BILLING_NUMBER"));
				details.setTid(tid.toUpperCase());
				break;
			}
			if (!isAmountAvailable)
				details.setMessage("failure. amount not available.");
		} catch (Exception e) {
			logger.error("fetchPayment method TID=" + tid, e);
			details.setMessage("failure. please try again later.");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchPayment method TID=" + tid, e);
				details.setMessage("failure. please try again later.");
			}
		}
		return details;
	}

	@Override
	public PaymentDetails updateResponse(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2,String rrn) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PaymentDetails retailDetails = new PaymentDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE SET STATUS=?,ADDNTL_ATTR1=?,ADDNTL_ATR2=?,RRN=? WHERE TID=? AND BILLING_NUMBER=? AND AMOUNT=? AND STATUS='initiate' ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, response);
			updateStmt.setString(2, addntl_attr1);
			updateStmt.setString(3, addntl_atr2);
			updateStmt.setString(4, rrn);
			updateStmt.setString(5, tid);
			updateStmt.setString(6, billingnumber);
			updateStmt.setString(7, amount);
			if(updateStmt.executeUpdate()==1){
			retailDetails.setMessage("success");
			retailDetails.setTid(tid);
			retailDetails.setAmount(amount);
			retailDetails.setBilling_number(billingnumber);
			retailDetails.setRrn(rrn);
			}else{
				retailDetails.setMessage("Data mismatch or not found. Please check");
				retailDetails.setTid(tid);
				retailDetails.setAmount(amount);
				retailDetails.setBilling_number(billingnumber);
				retailDetails.setRrn(rrn);
			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setMessage("failure. please try again later.");
			retailDetails.setTid(tid);
			retailDetails.setAmount(amount);
			retailDetails.setBilling_number(billingnumber);
			retailDetails.setRrn(rrn);
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}

	@Override
	public StatusDetails statusCheck(String tid, String amount, String billingnumber) {
		StatusDetails statusDetails = new StatusDetails();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		String status = "";
		String rrn = "";
		boolean isDataExits = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT STATUS,RRN FROM RETAIL_TABLE WHERE TID=? AND AMOUNT=? AND BILLING_NUMBER=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			pstmt.setString(1, tid);
			pstmt.setString(2, amount);
			pstmt.setString(3, billingnumber);
			resultSet = pstmt.executeQuery();
			
			while (resultSet.next()) {
				status = resultSet.getString("STATUS");
				rrn =resultSet.getString("RRN");
				isDataExits = true;
				break;
			}
			if(!isDataExits){
				statusDetails.setTid(tid);
				statusDetails.setAmount(amount);
				statusDetails.setBillingnumber(billingnumber);
				statusDetails.setRrn(rrn);
				statusDetails.setStatus("Data mismatch or Record Notfound");
			}else{			
			statusDetails.setTid(tid);
			statusDetails.setAmount(amount);
			statusDetails.setBillingnumber(billingnumber);
			statusDetails.setRrn(rrn);
			statusDetails.setStatus(status);
			}

		} catch (Exception e) {
			logger.error("statusCheck method TID=" + tid, e);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchBillingStatus method TID=" + tid, e);
			}
		}
		return statusDetails;
	}

	public boolean checkIsRecordAlreadyAvailable(String tid, String billingnumber,String amount) {

		Connection conn = null;
		PreparedStatement fetchProfileStmt = null;
		ResultSet result = null;
		boolean isRecordAvail = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			fetchProfileStmt = conn.prepareStatement("SELECT * FROM RETAIL_TABLE WHERE TID=? AND BILLING_NUMBER=? AND AMOUNT=? ");
			fetchProfileStmt.setString(1, tid);
			fetchProfileStmt.setString(2, billingnumber);
			fetchProfileStmt.setString(3, amount);
			result = fetchProfileStmt.executeQuery();

			while (result.next()) {
				isRecordAvail = true;
				break;
			}

		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailable]", e);
		}
		return isRecordAvail;

	}

	@Override
	public Object deletePayment(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		PaymentDetails details = new PaymentDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall("DELETE FROM RETAIL_TABLE WHERE TID=? AND STATUS='initiate'");
			pstmt.setString(1, tid);
		    pstmt.executeUpdate();
				details.setMessage("success");
		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailable]", e);
		}
		return details;
	}

	@Override
	public RetailType3Details updateResponseType3(RetailType3Details retailDetails) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		try {
			conn = new DBConnectionProd().getConnection(4);
			insertStmt = conn.prepareStatement(
					"INSERT INTO RETAIL_TABLE_TYPE3 (STATUS,BILLING_NUMBER,AMOUNT,CARD_SCHEME,TXN_DATE,TXN_TIME,MID,MASKED_CARD_NUM,TXN_TYPE,ADDNTL_ATTR1,ADDNTL_ATTR2,ADDNTL_ATTR3,TID,RRN) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);") ;

			insertStmt.setString(1, retailDetails.getStatus());
			insertStmt.setString(2, retailDetails.getBilling_number());
			insertStmt.setString(3, new Utils().getDecimal(retailDetails.getAmount()));
			insertStmt.setString(4, retailDetails.getCard_scheme());
			insertStmt.setString(5, retailDetails.getTxn_date());
			insertStmt.setString(6, retailDetails.getTxn_time());
			insertStmt.setString(7, retailDetails.getMid());
			insertStmt.setString(8, retailDetails.getMasked_card_number());
			insertStmt.setString(9, retailDetails.getTxn_type());
			insertStmt.setString(10, retailDetails.getAddntl_attr1());
			insertStmt.setString(11, retailDetails.getAddntl_attr2());
			insertStmt.setString(12, retailDetails.getAddntl_attr3());
			insertStmt.setString(13, retailDetails.getTid());
			insertStmt.setString(14, retailDetails.getRrn());
			//if check.. record is update 
			if(insertStmt.executeUpdate()==1){
			retailDetails.setMessage("success");
			retailDetails.setTid(retailDetails.getTid());
			retailDetails.setAmount(retailDetails.getAmount());
			retailDetails.setBilling_number(retailDetails.getBilling_number());
			retailDetails.setRrn(retailDetails.getRrn());
			retailDetails.setTxn_date(retailDetails.getTxn_date());
			retailDetails.setTxn_time(retailDetails.getTxn_time());
			retailDetails.setMid(retailDetails.getMid());
			retailDetails.setTxn_type(retailDetails.getTxn_type());
			retailDetails.setMasked_card_number(retailDetails.getMasked_card_number());
			retailDetails.setCard_scheme(retailDetails.getCard_scheme());
			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setMessage("failure. please try again later.");
			retailDetails.setTid(retailDetails.getTid());
			retailDetails.setAmount(retailDetails.getAmount());
			retailDetails.setBilling_number(retailDetails.getBilling_number());
			retailDetails.setRrn(retailDetails.getRrn());
			retailDetails.setTxn_date(retailDetails.getTxn_date());
			retailDetails.setTxn_time(retailDetails.getTxn_time());
			retailDetails.setMid(retailDetails.getMid());
			retailDetails.setTxn_type(retailDetails.getTxn_type());
			retailDetails.setMasked_card_number(retailDetails.getMasked_card_number());
			retailDetails.setCard_scheme(retailDetails.getCard_scheme());
		} finally {
			try {
				if (insertStmt != null)
					insertStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}

	@Override
	public  List<RetailType3Details> fetchDetailType3(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		List<RetailType3Details> retaillist = new ArrayList<RetailType3Details>();
        try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT * FROM RETAIL_TABLE_TYPE3 WHERE TID=? ");

			pstmt.setString(1, tid);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
		        RetailType3Details details = new RetailType3Details();
                details.setMessage("Record fetched Successfully");
          details.setUrn(resultSet.getString("URN"));
          details.setTid(resultSet.getString("TID"));
          details.setAmount(resultSet.getString("AMOUNT"));
          details.setStatus(resultSet.getString("STATUS"));
          details.setBilling_number(resultSet.getString("BILLING_NUMBER"));
          details.setRrn(resultSet.getString("RRN"));
          details.setCard_scheme(resultSet.getString("CARD_SCHEME"));
          details.setMid(resultSet.getString("MID"));
          details.setMasked_card_number(resultSet.getString("MASKED_CARD_NUM"));
          details.setTxn_type(resultSet.getString("TXN_TYPE"));
          details.setAddntl_attr1(resultSet.getString("ADDNTL_ATTR1"));
          details.setAddntl_attr2(resultSet.getString("ADDNTL_ATTR2"));
          details.setAddntl_attr3(resultSet.getString("ADDNTL_ATTR3"));
          logger.info("URN Details fetched Successfully");
          retaillist.add(details);
			}
		}catch (Exception e) {
			logger.error("fetchtype3 method TID=" + tid, e);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchtype3 method TID=" + tid, e);
			}
		}
		return retaillist;
	}

	@Override
	public ReturnObject sendSMS(String mobile_number, String content) {
		ReturnObject returnObject= new ReturnObject();
		try {
			StringBuffer response = Utils.sendSMSNew(mobile_number, content);
			//if check response.. status is zero or not.
			if (response.toString() != null && response.toString().contains("<status>0</status>")) {
				returnObject.setReturnCode(0);
				returnObject.setReturnMessage("Success");
				//if status value is -5 calling retry method
			}else if (response.toString() != null && response.toString().contains("<status>-5</status>")) {
				logger.error("Error getting in SMS method :" +response);
				response = Utils.sendSMSNew(mobile_number, content);
				if (response.toString() != null && response.toString().contains("<status>0</status>")) {
					returnObject.setReturnCode(0);
					returnObject.setReturnMessage("Success");
				}
			}
		} catch (Exception e) {
			logger.error("Error getting in SMS notification: mobileNumber=" + mobile_number, e);
		}
		return returnObject;
	}

	@Override
	public RetailType4Details fetchDetailType4(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		RetailType4Details details = new RetailType4Details();
		boolean isAmountAvailable = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareStatement("select * from RETAIL_TABLE_TYPE4 where tid=? and (status = 'INITIATE' or status = 'FETCHED')  order by insert_timestamp desc limit 1");
			pstmt.setString(1, tid);
			resultSet = pstmt.executeQuery();
			
			
			while (resultSet.next()) {
			isAmountAvailable = true;
			if(resultSet.getString("AMOUNT").isEmpty() && StringUtils.isBlank(resultSet.getString("AMOUNT")) && resultSet.getString("AMOUNT")==null){
		          details.setTid(resultSet.getString("TID"));
		          details.setResponse_code(1);
		          details.setResponse_message("Failure. amount not available");
			}
			else{
					details.setResponse_code(0);
					details.setResponse_message("Record fetched Successfully");
		          details.setUrn(resultSet.getString("URN"));
		          details.setTid(resultSet.getString("TID"));
		          details.setAmount(resultSet.getString("AMOUNT"));		
		          details.setStatus(resultSet.getString("STATUS"));
		          details.setBilling_number(resultSet.getString("BILLING_NUMBER"));
		          details.setRrn(resultSet.getString("RRN"));
		          details.setCard_scheme(resultSet.getString("CARD_SCHEME"));
		          details.setMid(resultSet.getString("MID"));
		          details.setMasked_card_number(resultSet.getString("masked_card_number"));
		          details.setTxn_type(resultSet.getString("TXN_TYPE"));
		          details.setAdditional_attribute1(resultSet.getString("additional_attribute1"));
		          details.setAdditional_attribute2(resultSet.getString("additional_attribute2"));
		          details.setAdditional_attribute3(resultSet.getString("additional_attribute3"));
		          logger.info("URN Details fetched Successfully");
			}
			}
			if (!isAmountAvailable) {
				details.setResponse_code(1);
				details.setResponse_message("Failure. amount not available");
			}
		}catch (Exception e) {
			logger.error("fetchtype4 method TID=" + tid, e);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchtype4 method TID=" + tid, e);
			}
		}
		return details;
	}
}
