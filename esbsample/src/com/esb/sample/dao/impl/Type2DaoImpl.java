package com.esb.sample.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.ManagedBean;

import org.apache.log4j.Logger;
import com.esb.sample.dao.Type2Dao;
import com.esb.sample.init.DBConnectionProd;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.util.Utils;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

@ManagedBean
public class Type2DaoImpl implements Type2Dao {

	final static Logger logger = Logger.getLogger(Type2DaoImpl.class);

	@Override
	public RetailType2Details retailType2response(String tid, String amount) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		ResultSet resultSet = null;
		RetailType2Details details = new RetailType2Details();
		boolean isAmountAvailable = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			insertStmt = conn.prepareStatement("INSERT INTO retail_type2 (TID, AMOUNT) VALUES (?, ?);");

			insertStmt.setString(1, tid);
			insertStmt.setString(2, new Utils().getDecimal(amount));
			insertStmt.executeUpdate();

			/*
			 * while (insertStmt.executeUpdate()==1) { isAmountAvailable = true;
			 * details.setMessage("success");
			 * details.setAmount(resultSet.getString("AMOUNT"));
			 * details.setUrn(resultSet.getString("URN"));
			 * details.setTid(tid.toUpperCase()); break; } if
			 * (!isAmountAvailable)
			 * details.setMessage("failure. amount not available.");
			 */
		} catch (Exception e) {
			logger.error("fetchPayment method TID=" + tid, e);
			details.setMessage("failure. please try again later.");
		} finally {
			try {
				if (insertStmt != null && !insertStmt.isClosed())
					insertStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchPayment method TID=" + tid, e);
				details.setMessage("failure. please try again later.");
			}
		}
		return details;

	}

	@Override
	public RetailType2Details addUrnDetail(String tid, String amount) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		PreparedStatement fetchStmt = null;
		RetailType2Details details = new RetailType2Details();
		details.setTid(tid);
		details.setAmount(amount);
		try {
			
			conn = new DBConnectionProd().getConnection(4);
			insertStmt = conn.prepareStatement("INSERT INTO RETAIL_TABLE_TYPE2 (TID, AMOUNT) VALUES (?, ?);");
			insertStmt.setString(1, tid.toUpperCase());
			insertStmt.setString(2, amount);
			int count = insertStmt.executeUpdate();
			if (count > 0) {
				String query = "SELECT MAX(URN) FROM RETAIL_TABLE_TYPE2;";
				Statement st = conn.createStatement();
				ResultSet record = st.executeQuery(query);

				if (record.next())
				fetchStmt = conn.prepareStatement("Select * from RETAIL_TABLE_TYPE2 where URN=" + record.getInt(1));
				ResultSet records = fetchStmt.executeQuery();
				if(records.next()){
				details.setTid(records.getString("TID"));
				details.setAmount(records.getString("AMOUNT"));
				details.setUrn(records.getInt("URN")+"");
				details.setBilling_number(records.getString("BILLING_NUMBER"));
				details.setMessage("success");
				logger.info("URN Details added Successfully");
				}else{
					details.setMessage("No Records found");
					logger.info("URN details not found");
				}
			}else{
				details.setMessage("URN detail insertion failed");
				logger.info("URN detail insertion failed");
			}
			
			

		} catch (Exception e) {
			logger.error("addUrnDetail method TID=" + tid, e);
			details.setMessage("failure. please try again later.");
		} finally {
			try {
				if (insertStmt != null && !insertStmt.isClosed())
					insertStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("addUrnDetail method TID=" + tid, e);
				details.setMessage("failure. please try again later.");
			}
		}
		return details;

	}

	@Override
	public ResDetailsForRetailType fetchUrnDetail(String urn) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		ResDetailsForRetailType details = new ResDetailsForRetailType();
		try {
			conn = new DBConnectionProd().getConnection(4);
			insertStmt = conn.prepareCall(
					"SELECT * FROM RETAIL_TABLE_TYPE2 WHERE URN=? and STATUS=?");

			insertStmt.setInt(1,Integer.parseInt(urn));
			insertStmt.setString(2, "initiate");
			ResultSet record = insertStmt.executeQuery();
					

			if (record.next()){
				details.setResponse_code(0);
				details.setResponse_message("Record fetched Successfully");
			details.setUrn(record.getString("URN"));
			details.setTid(record.getString("TID"));
			details.setAmount(record.getString("AMOUNT"));
			details.setStatus(record.getString("STATUS"));
			details.setBilling_number(record.getString("BILLING_NUMBER"));
			details.setRrn(record.getString("RRN"));
			details.setCard_scheme(record.getString("CARD_SCHEME"));
			details.setMid(record.getString("MID"));
			details.setMasked_card_number(record.getString("MASKED_CARD_NUM"));
			details.setTxn_type(record.getString("TXN_TYPE"));
			details.setAdditional_attribute1(record.getString("ADDNTL_ATTR1"));
			details.setAdditional_attribute2(record.getString("ADDNTL_ATTR2"));
			details.setAdditional_attribute3(record.getString("ADDNTL_ATTR3"));
			details.setOrganization_code(record.getString("ORGANIZATION_CODE"));
			details.setTxn_date(record.getString("TXN_DATE"));
			details.setTxn_time(record.getString("TXN_TIME"));
			logger.info("URN Details fetched Successfully");
			}else{
				details.setResponse_code(1);
				details.setResponse_message("No Records found");
				logger.info("No Records found");
			}

		} catch (Exception e) {
			logger.error("fetchDetailType2 method URN=" + urn, e);
			details.setResponse_code(1);
			details.setResponse_message("No Records found");
		} finally {
			try {
				if (insertStmt != null && !insertStmt.isClosed())
					insertStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchDetailType2 method URN=" + urn, e);
				details.setResponse_code(1);
				details.setResponse_message("failure. please try again later.");
			}
		}
		return details;

	}
}
