package com.esb.sample.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.annotation.ManagedBean;

import org.apache.log4j.Logger;

import com.esb.sample.dao.SampleDaoNew;
import com.esb.sample.init.DBConnectionProd;
import com.esb.sample.model.InsertReqDetails;
import com.esb.sample.model.InsertResponseDetails;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.PaymentDetailsold;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.model.StatusDetails;
import com.esb.sample.util.Utils;

@ManagedBean
public class SampleDaoImplNew implements SampleDaoNew {

	final static Logger logger = Logger.getLogger(SampleDaoImpl.class);

	@Override
	public InsertResponseDetails initiatePaymentNew(InsertReqDetails retailDetail) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		PreparedStatement fetchStmt = null;
		InsertResponseDetails paymentDetails = new InsertResponseDetails();
		// here I am checking if tid is already present
		boolean isRecordAvail = new SampleDaoImplNew().checkIsRecordAlreadyAvailableNew(retailDetail);

		// if record is not present then insert new record
		if (!isRecordAvail) {
			try {
				conn = new DBConnectionProd().getConnection(4);
				insertStmt = conn.prepareStatement(
						"INSERT INTO RETAIL_TABLE_TYPE1 (TID, AMOUNT, BILLING_NUMBER, ADDNTL_ATTR1, ADDNTL_ATTR2, ADDNTL_ATTR3, ORGANIZATION_CODE) VALUES (?, ?, ?, ?, ?, ?, ?);");

				insertStmt.setString(1, retailDetail.getTid().toUpperCase());
				insertStmt.setString(2, new Utils().getDecimal(retailDetail.getAmount()));
				insertStmt.setString(3, retailDetail.getBilling_number());
				insertStmt.setString(4, retailDetail.getAdditional_attribute1());
				insertStmt.setString(5, retailDetail.getAdditional_attribute2());
				insertStmt.setString(6, retailDetail.getAdditional_attribute3());
				insertStmt.setString(7, retailDetail.getOrganization_code());
				int count=insertStmt.executeUpdate();
				if (count > 0) {
					String query = "SELECT MAX(URN) FROM RETAIL_TABLE_TYPE1;";
					Statement st = conn.createStatement();
					ResultSet record = st.executeQuery(query);

					if (record.next())
					fetchStmt = conn.prepareStatement("Select * from RETAIL_TABLE_TYPE1 where URN=" + record.getInt(1));
					ResultSet records = fetchStmt.executeQuery();
					if(records.next()){
						paymentDetails.setResponse_code(0);
						paymentDetails.setResponse_message("success");
						paymentDetails.setTid(records.getString("TID"));
						paymentDetails.setAmount(records.getString("AMOUNT"));
						paymentDetails.setOrganization_code(records.getString("ORGANIZATION_CODE"));
						paymentDetails.setUrn(records.getInt("URN")+"");
						paymentDetails.setBilling_number(records.getString("BILLING_NUMBER"));
						paymentDetails.setAdditional_attribute1(records.getString("ADDNTL_ATTR1"));
						paymentDetails.setAdditional_attribute2(records.getString("ADDNTL_ATTR2"));
						paymentDetails.setAdditional_attribute3(records.getString("ADDNTL_ATTR3"));
					logger.info("URN Details added Successfully");
					}else{
						paymentDetails.setResponse_code(1);
						paymentDetails.setResponse_message("No Records found");
						logger.info("URN details not found");
					}
				}else{
					paymentDetails.setResponse_code(1);
					paymentDetails.setResponse_message("URN detail insertion failed");
					logger.info("URN detail insertion failed");
				}
							

			} catch (Exception e) {
				logger.error("Error in initiatePayment method", e);
			} finally {
				try {
					if (insertStmt != null)
						insertStmt.close();
					new DBConnectionProd().closeConnection(conn);
				} catch (SQLException e) {
					logger.error("Error in initiatePayment method", e);
				}
			}
			// if record already present then update record
		} else {
			 paymentDetails = new SampleDaoImplNew().updateStatusNew(retailDetail);
		}
		return paymentDetails;
	}
	
	@Override
	public InsertResponseDetails initiatePaymentType2(InsertReqDetails retailDetail) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		PreparedStatement fetchStmt = null;
		InsertResponseDetails paymentDetails = new InsertResponseDetails();
		
			try {
				conn = new DBConnectionProd().getConnection(4);
				insertStmt = conn.prepareStatement(
						"INSERT INTO RETAIL_TABLE_TYPE2 (TID, AMOUNT, BILLING_NUMBER, ADDNTL_ATTR1, ADDNTL_ATTR2, ADDNTL_ATTR3, ORGANIZATION_CODE) VALUES (?, ?, ?, ?, ?, ?, ?);");

				insertStmt.setString(1, retailDetail.getTid().toUpperCase());
				insertStmt.setString(2, new Utils().getDecimal(retailDetail.getAmount()));
				insertStmt.setString(3, retailDetail.getBilling_number());
				insertStmt.setString(4, retailDetail.getAdditional_attribute1());
				insertStmt.setString(5, retailDetail.getAdditional_attribute2());
				insertStmt.setString(6, retailDetail.getAdditional_attribute3());
				insertStmt.setString(7, retailDetail.getOrganization_code());
				int count=insertStmt.executeUpdate();
				if (count > 0) {
					String query = "SELECT MAX(URN) FROM RETAIL_TABLE_TYPE2;";
					Statement st = conn.createStatement();
					ResultSet record = st.executeQuery(query);

					if (record.next())
					fetchStmt = conn.prepareStatement("Select * from RETAIL_TABLE_TYPE2 where URN=" + record.getInt(1));
					ResultSet records = fetchStmt.executeQuery();
					if(records.next()){
						paymentDetails.setResponse_code(0);
						paymentDetails.setResponse_message("success");
						paymentDetails.setTid(records.getString("TID"));
						paymentDetails.setAmount(records.getString("AMOUNT"));
						paymentDetails.setOrganization_code(records.getString("ORGANIZATION_CODE"));
						paymentDetails.setUrn(records.getInt("URN")+"");
						paymentDetails.setBilling_number(records.getString("BILLING_NUMBER"));
						paymentDetails.setAdditional_attribute1(records.getString("ADDNTL_ATTR1"));
						paymentDetails.setAdditional_attribute2(records.getString("ADDNTL_ATTR2"));
						paymentDetails.setAdditional_attribute3(records.getString("ADDNTL_ATTR3"));
					logger.info("URN Details added Successfully");
					}else{
						paymentDetails.setResponse_code(1);
						paymentDetails.setResponse_message("No Records found");
						logger.info("URN details not found");
					}
				}else{
					paymentDetails.setResponse_code(1);
					paymentDetails.setResponse_message("URN detail insertion failed");
					logger.info("URN detail insertion failed");
				}
							

			} catch (Exception e) {
				logger.error("Error in initiatePayment method", e);
			} finally {
				try {
					if (insertStmt != null)
						insertStmt.close();
					new DBConnectionProd().closeConnection(conn);
				} catch (SQLException e) {
					logger.error("Error in initiatePayment method", e);
				}
			}
			
		return paymentDetails;
	}

	
	public InsertResponseDetails updateStatusNew(InsertReqDetails retailDetail) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PreparedStatement fetchStmt = null;
		InsertResponseDetails paymentDetails = new InsertResponseDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_TYPE1 SET BILLING_NUMBER=?, AMOUNT=?, ORGANIZATION_CODE=?, ADDNTL_ATTR1=?, ADDNTL_ATTR2=?, ADDNTL_ATTR3=? WHERE TID=? and STATUS=?");

			
			updateStmt.setString(1, retailDetail.getBilling_number());
			updateStmt.setString(2, new Utils().getDecimal(retailDetail.getAmount()));
			updateStmt.setString(3, retailDetail.getOrganization_code());
			updateStmt.setString(4, retailDetail.getAdditional_attribute1());
			updateStmt.setString(5, retailDetail.getAdditional_attribute2());
			updateStmt.setString(6, retailDetail.getAdditional_attribute3());			
			updateStmt.setString(7, retailDetail.getTid());
			updateStmt.setString(8, "initiate");

			int count=updateStmt.executeUpdate();
			if(count>0){
				fetchStmt = conn.prepareStatement("Select * from RETAIL_TABLE_TYPE1 WHERE TID=? order by INSERT_TIMESTAMP desc limit 1 ");
				fetchStmt.setString(1, retailDetail.getTid());
				ResultSet records = fetchStmt.executeQuery();
				if(records.next()){
					paymentDetails.setResponse_code(0);
					paymentDetails.setResponse_message("success");
					paymentDetails.setTid(records.getString("TID"));
					paymentDetails.setAmount(records.getString("AMOUNT"));
					paymentDetails.setOrganization_code(records.getString("ORGANIZATION_CODE"));
					paymentDetails.setUrn(records.getInt("URN")+"");
					paymentDetails.setBilling_number(records.getString("BILLING_NUMBER"));
					paymentDetails.setAdditional_attribute1(records.getString("ADDNTL_ATTR1"));
					paymentDetails.setAdditional_attribute2(records.getString("ADDNTL_ATTR2"));
					paymentDetails.setAdditional_attribute3(records.getString("ADDNTL_ATTR3"));
				logger.info("URN Details added Successfully");
				}
			}
			

		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			paymentDetails.setResponse_code(1);
			paymentDetails.setResponse_message("failure. please try again later.");
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return paymentDetails;
	}
	
	/*public PaymentDetails updateStatusType2(RetailType2Details retailDetail) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PreparedStatement fetchStmt = null;
		PaymentDetails paymentDetails = new PaymentDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_TYPE2 SET STATUS=?,BILLING_NUMBER=?,AMOUNT=?,ADDNTL_ATTR1=?,ADDNTL_ATTR2=? WHERE TID=? and RRN=?");

			updateStmt.setString(1, "initiate");
			updateStmt.setString(2, retailDetail.getBilling_number());
			updateStmt.setString(3, new Utils().getDecimal(retailDetail.getAmount()));
			updateStmt.setString(4, retailDetail.getAddntl_attr1());
			updateStmt.setString(5, retailDetail.getAddntl_attr2());
			updateStmt.setString(6, retailDetail.getTid());
			updateStmt.setString(7, "0");
			int count=updateStmt.executeUpdate();
			if(count>0){
				fetchStmt = conn.prepareStatement("Select * from RETAIL_TABLE_TYPE2 WHERE TID=? and RRN=?");
				fetchStmt.setString(1, retailDetail.getTid());
				fetchStmt.setString(2,"0");
				ResultSet records = fetchStmt.executeQuery();
				if(records.next()){
					paymentDetails.setTid(records.getString("TID"));
					paymentDetails.setAmount(records.getString("AMOUNT"));
					paymentDetails.setUrn(records.getInt("URN")+"");
					paymentDetails.setBilling_number(records.getString("BILLING_NUMBER"));
					paymentDetails.setMessage("success");
				logger.info("URN Details added Successfully");
				}
			}
			

		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			paymentDetails.setMessage("failure. please try again later.");
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return paymentDetails;
	}
*/
	@Override
	public ArrayList<ResDetailsForRetailType> fetchPaymentNew(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ResDetailsForRetailType detail = new ResDetailsForRetailType();
		boolean isAmountAvailable = false;
		ArrayList<ResDetailsForRetailType> arrayList=new ArrayList<ResDetailsForRetailType>();
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT * FROM RETAIL_TABLE_TYPE1 WHERE TID=? and STATUS=? ORDER BY INSERT_TIMESTAMP DESC");

			pstmt.setString(1, tid.toUpperCase());
			pstmt.setString(2, "initiate");
			resultSet = pstmt.executeQuery();
			
			while (resultSet.next()) {
				isAmountAvailable = true;
				ResDetailsForRetailType details = new ResDetailsForRetailType();
				details.setResponse_code(0);
				details.setResponse_message("Record fetched Successfully");
				details.setTid(resultSet.getString("TID"));
				details.setAmount(resultSet.getString("AMOUNT"));
				details.setBilling_number(resultSet.getString("BILLING_NUMBER"));
				details.setRrn(resultSet.getString("RRN"));
				details.setUrn(resultSet.getString("URN"));
				details.setAdditional_attribute1(resultSet.getString("ADDNTL_ATTR1"));
				details.setAdditional_attribute2(resultSet.getString("ADDNTL_ATTR2"));
				details.setAdditional_attribute3(resultSet.getString("ADDNTL_ATTR3"));
				details.setOrganization_code(resultSet.getString("ORGANIZATION_CODE"));
				details.setStatus(resultSet.getString("STATUS"));
				details.setCard_scheme(resultSet.getString("CARD_SCHEME"));
				details.setTxn_date(resultSet.getString("TXN_DATE"));
				details.setTxn_time(resultSet.getString("TXN_TIME"));
				details.setMid(resultSet.getString("MID"));
				details.setMasked_card_number(resultSet.getString("MASKED_CARD_NUM"));
				details.setTxn_type(resultSet.getString("TXN_TYPE"));
				arrayList.add(details);
				
			}
			// if check.. amount is not available
			if (!isAmountAvailable){
				detail.setResponse_code(1);
				detail.setResponse_message("failure. No Record Found.");
			arrayList.add(detail);
			}
		} catch (Exception e) {
			logger.error("fetchPayment method TID=" + tid, e);
			detail.setResponse_code(1);
			detail.setResponse_message("failure. please try again later.");
			arrayList.add(detail);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchPayment method TID=" + tid, e);
				detail.setResponse_code(1);
				detail.setResponse_message("failure. please try again later.");
				arrayList.add(detail);
			}
		}
		return arrayList;
	}

	/*@Override
	public PaymentDetails updateResponseNew(RetailType2Details retailDetail) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PaymentDetails retailDetails = new PaymentDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_NEW SET STATUS=?,RRN=?,BILLING_NUMBER=?,AMOUNT=?,ADDNTL_ATTR1=?,ADDNTL_ATR2=? WHERE TID=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, retailDetail.getStatus());
			updateStmt.setString(2, retailDetail.getRrn());
			updateStmt.setString(3, retailDetail.getBilling_number());
			updateStmt.setString(4, retailDetail.getAmount());
			updateStmt.setString(5, retailDetail.getAddntl_attr1());
			updateStmt.setString(6, retailDetail.getAddntl_attr2());
			updateStmt.setString(7, retailDetail.getTid().toUpperCase());

			// if check.. record is update
			retailDetails.setTid(retailDetail.getTid());
			retailDetails.setAmount(retailDetail.getAmount());
			retailDetails.setBillingnumber(retailDetail.getBilling_number());
			retailDetails.setRrn(retailDetail.getRrn());
			if (updateStmt.executeUpdate() == 1) {
				retailDetails.setMessage("success");

			} else {
				retailDetails.setMessage("Data mismatch or not found. Please check");

			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setMessage("failure. please try again later.");

		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}*/
	
	
	@Override
	public ResDetailsForRetailType updateResponseNew(RetailType2Details retailDetail) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		ResDetailsForRetailType retailDetails = new ResDetailsForRetailType();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_TYPE1 SET TID=?,AMOUNT=?,STATUS=?,BILLING_NUMBER=?,RRN=?,CARD_SCHEME=?,TXN_DATE=?,TXN_TIME=?,MID=?,MASKED_CARD_NUM=?,TXN_TYPE=?,ADDNTL_ATTR1=?,ADDNTL_ATTR2=?,ADDNTL_ATTR3=? WHERE URN=?");

			updateStmt.setString(1, retailDetail.getTid().toUpperCase());
			updateStmt.setString(2, new Utils().getDecimal(retailDetail.getAmount()));
			updateStmt.setString(3, retailDetail.getStatus());
			updateStmt.setString(4, retailDetail.getBilling_number());
			updateStmt.setString(5, retailDetail.getRrn());
			updateStmt.setString(6, retailDetail.getCard_scheme());
			updateStmt.setString(7, retailDetail.getTxn_date());
			updateStmt.setString(8, retailDetail.getTxn_time());
			updateStmt.setString(9, retailDetail.getMid());
			updateStmt.setString(10, retailDetail.getMasked_card_number());
			updateStmt.setString(11, retailDetail.getTxn_type());
			updateStmt.setString(12, retailDetail.getAddntl_attr1());
			updateStmt.setString(13, retailDetail.getAddntl_attr2());
			updateStmt.setString(14, retailDetail.getAddntl_attr3());
			updateStmt.setString(15, retailDetail.getUrn());
			// if check.. record is update
			retailDetails.setTid(retailDetail.getTid());
			retailDetails.setAmount(retailDetail.getAmount());
			retailDetails.setStatus(retailDetail.getStatus());
			retailDetails.setBilling_number(retailDetail.getBilling_number());
			retailDetails.setRrn(retailDetail.getRrn());
			retailDetails.setUrn(retailDetail.getUrn());
			retailDetails.setCard_scheme(retailDetail.getCard_scheme());
			retailDetails.setTxn_date(retailDetail.getTxn_date());
			retailDetails.setTxn_time(retailDetail.getTxn_time());
			retailDetails.setMid(retailDetail.getMid());
			retailDetails.setMasked_card_number(retailDetail.getMasked_card_number());
			retailDetails.setTxn_type(retailDetail.getTxn_type());
			retailDetails.setAdditional_attribute1("");
			retailDetails.setAdditional_attribute2("");
			retailDetails.setAdditional_attribute3("");
			retailDetails.setOrganization_code("");
			
			if (updateStmt.executeUpdate() == 1) {
				retailDetails.setResponse_code(0);
				retailDetails.setResponse_message("success");

			} else {
				retailDetails.setResponse_code(1);
				retailDetails.setResponse_message("Data mismatch or not found. Please check");

			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setResponse_code(1);
			retailDetails.setResponse_message("failure. please try again later.");

		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}

	@Override
	public ResDetailsForRetailType updateResponseType2(RetailType2Details retailDetail) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		ResDetailsForRetailType retailDetails = new ResDetailsForRetailType();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_TYPE2 SET TID=?,AMOUNT=?,STATUS=?,BILLING_NUMBER=?,RRN=?,CARD_SCHEME=?,TXN_DATE=?,TXN_TIME=?,MID=?,MASKED_CARD_NUM=?,TXN_TYPE=?,ADDNTL_ATTR1=?,ADDNTL_ATTR2=?,ADDNTL_ATTR3=? WHERE URN=?");

			updateStmt.setString(1, retailDetail.getTid().toUpperCase());
			updateStmt.setString(2, new Utils().getDecimal(retailDetail.getAmount()) );
			updateStmt.setString(3, retailDetail.getStatus());
			updateStmt.setString(4, retailDetail.getBilling_number());
			updateStmt.setString(5, retailDetail.getRrn());
			updateStmt.setString(6, retailDetail.getCard_scheme());
			updateStmt.setString(7, retailDetail.getTxn_date());
			updateStmt.setString(8, retailDetail.getTxn_time());
			updateStmt.setString(9, retailDetail.getMid());
			updateStmt.setString(10, retailDetail.getMasked_card_number());
			updateStmt.setString(11, retailDetail.getTxn_type());
			updateStmt.setString(12, retailDetail.getAddntl_attr1());
			updateStmt.setString(13, retailDetail.getAddntl_attr2());
			updateStmt.setString(14, retailDetail.getAddntl_attr3());
			updateStmt.setString(15, retailDetail.getUrn());
			// if check.. record is update
			retailDetails.setTid(retailDetail.getTid());
			retailDetails.setAmount(retailDetail.getAmount());
			retailDetails.setStatus(retailDetail.getStatus());
			retailDetails.setBilling_number(retailDetail.getBilling_number());
			retailDetails.setRrn(retailDetail.getRrn());
			retailDetails.setUrn(retailDetail.getUrn());
			retailDetails.setCard_scheme(retailDetail.getCard_scheme());
			retailDetails.setTxn_date(retailDetail.getTxn_date());
			retailDetails.setTxn_time(retailDetail.getTxn_time());
			retailDetails.setMid(retailDetail.getMid());
			retailDetails.setMasked_card_number(retailDetail.getMasked_card_number());
			retailDetails.setTxn_type(retailDetail.getTxn_type());
			retailDetails.setAdditional_attribute1("");
			retailDetails.setAdditional_attribute2("");
			retailDetails.setAdditional_attribute3("");
			retailDetails.setOrganization_code("");

			if (updateStmt.executeUpdate() == 1) {
				retailDetails.setResponse_code(0);
				retailDetails.setResponse_message("success");

			} else {
				retailDetails.setResponse_code(1);
				retailDetails.setResponse_message("Data mismatch or not found. Please check");

			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setResponse_code(1);
			retailDetails.setResponse_message("failure. please try again later.");

		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}

	public boolean checkIsRecordAlreadyAvailableNew(InsertReqDetails retailDetail) {

		Connection conn = null;
		PreparedStatement fetchProfileStmt = null;
		ResultSet result = null;
		boolean isRecordAvail = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			fetchProfileStmt = conn.prepareStatement("SELECT * FROM RETAIL_TABLE_TYPE1 WHERE TID=? and STATUS=?");
			fetchProfileStmt.setString(1, retailDetail.getTid());
			fetchProfileStmt.setString(2, "initiate");
			result = fetchProfileStmt.executeQuery();

			while (result.next()) {
				isRecordAvail = true;
				break;
			}

		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailableNew]", e);
		}
		return isRecordAvail;

	}
	
	/*public boolean checkIsRecordAlreadyAvailableType2(RetailType2Details retailDetail) {

		Connection conn = null;
		PreparedStatement fetchProfileStmt = null;
		ResultSet result = null;
		boolean isRecordAvail = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			fetchProfileStmt = conn.prepareStatement("SELECT * FROM RETAIL_TABLE_TYPE2 WHERE TID=? and STATUS=? and RRN=?");
			fetchProfileStmt.setString(1, retailDetail.getTid());
			fetchProfileStmt.setString(2, "initiate");
			fetchProfileStmt.setString(3, "0");
			result = fetchProfileStmt.executeQuery();

			while (result.next()) {
				isRecordAvail = true;
				break;
			}

		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailableNew]", e);
		}
		return isRecordAvail;

	}*/
	public boolean checkIsRecordAlreadyAvailableType2(String tid) {

		Connection conn = null;
		PreparedStatement fetchProfileStmt = null;
		ResultSet result = null;
		boolean isRecordAvail = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			fetchProfileStmt = conn.prepareStatement("SELECT * FROM RETAIL_TABLE_TYPE2 WHERE TID=?");
			fetchProfileStmt.setString(1, tid);
			result = fetchProfileStmt.executeQuery();

			while (result.next()) {
				isRecordAvail = true;
				break;
			}

		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailableType2]", e);
		}
		return isRecordAvail;

	}
	
	@Override
	public PaymentDetailsold showinitiatePaymentNew(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber,String rrn) {
		Connection conn = null;
		PreparedStatement insertStmt = null;
		PaymentDetailsold paymentDetails = new PaymentDetailsold();
		//here I am checking if tid is already present
		boolean isRecordAvail = new SampleDaoImplNew().checkIsRecordAlreadyAvailableNew(tid);
		
		//if record is not present then insert new record
		if (!isRecordAvail) {
			try {
				conn = new DBConnectionProd().getConnection(4);
				insertStmt = conn.prepareStatement(
						"INSERT INTO RETAIL_TABLE_NEW (TID, AMOUNT, BILLING_NUMBER, ADDNTL_ATTR1, ADDNTL_ATR2,RRN) VALUES (?, ?, ?, ?, ?, ?);");

				insertStmt.setString(1, tid);
				insertStmt.setString(2, new Utils().getDecimal(amount));
				insertStmt.setString(3, billingnumber);
				insertStmt.setString(4, addntl_attr1);
				insertStmt.setString(5, addntl_atr2);
				insertStmt.setString(6, "0");
				insertStmt.executeUpdate();
				paymentDetails.setMessage("success");
				paymentDetails.setTid(tid);
				paymentDetails.setAmount(amount);
				paymentDetails.setBillingnumber(billingnumber);

			} catch (Exception e) {
				logger.error("Error in initiatePayment method", e);
			} finally {
				try {
					if (insertStmt != null)
						insertStmt.close();
					new DBConnectionProd().closeConnection(conn);
				} catch (SQLException e) {
					logger.error("Error in initiatePayment method", e);
				}
			}
			//if record already present then update record
		} else {
			boolean isUpdate = new SampleDaoImplNew().showupdateStatusNew(tid, amount, "initiate", billingnumber, addntl_attr1,
					addntl_atr2);

			if (isUpdate) {
				paymentDetails.setMessage("success");
				paymentDetails.setTid(tid);
				paymentDetails.setAmount(amount);
				paymentDetails.setBillingnumber(billingnumber);
			}
		}
		return paymentDetails;
	}

	@Override
	public boolean showupdateStatusNew(String tid, String amount, String response, String billingnumber, String addntl_attr1,
			String addntl_atr2) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PaymentDetails paymentDetails = new PaymentDetails();
		boolean isUpdate = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_NEW SET STATUS=?,BILLING_NUMBER=?,AMOUNT=?,ADDNTL_ATTR1=?,ADDNTL_ATR2=? WHERE TID=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, response);
			updateStmt.setString(2, billingnumber);
			updateStmt.setString(3, amount);
			updateStmt.setString(4, addntl_attr1);
			updateStmt.setString(5, addntl_atr2);
			updateStmt.setString(6, tid);
			updateStmt.executeUpdate();
			isUpdate = true;

		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			paymentDetails.setMessage("failure. please try again later.");
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return isUpdate;
	}

	@Override
	public PaymentDetailsold showfetchPaymentNew(String tid) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		PaymentDetailsold details = new PaymentDetailsold();
		boolean isAmountAvailable = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT AMOUNT,BILLING_NUMBER FROM RETAIL_TABLE_NEW WHERE TID=? ORDER BY INSERT_TIMESTAMP DESC");

			pstmt.setString(1, tid.toUpperCase());
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				isAmountAvailable = true;
				details.setMessage("success");
				details.setAmount(resultSet.getString("AMOUNT"));
				details.setBillingnumber(resultSet.getString("BILLING_NUMBER"));
				details.setTid(tid.toUpperCase());
				break;
			}
			//if check.. amount is not available
			if (!isAmountAvailable)
				details.setMessage("failure. amount not available.");
		} catch (Exception e) {
			logger.error("fetchPayment method TID=" + tid, e);
			details.setMessage("failure. please try again later.");
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchPayment method TID=" + tid, e);
				details.setMessage("failure. please try again later.");
			}
		}
		return details;
	}

	@Override
	public PaymentDetailsold showupdateResponseNew(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2,String rrn) {
		Connection conn = null;
		PreparedStatement updateStmt = null;
		PaymentDetailsold retailDetails = new PaymentDetailsold();
		try {
			conn = new DBConnectionProd().getConnection(4);
			updateStmt = conn.prepareStatement(
					"UPDATE RETAIL_TABLE_NEW SET STATUS=?,RRN=?,BILLING_NUMBER=?,AMOUNT=?,ADDNTL_ATTR1=?,ADDNTL_ATR2=? WHERE TID=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			updateStmt.setString(1, response);
			updateStmt.setString(2, rrn);
			updateStmt.setString(3, billingnumber);
			updateStmt.setString(4, amount);
			updateStmt.setString(5, addntl_attr1);
			updateStmt.setString(6, addntl_atr2);
			updateStmt.setString(7, tid);
			//if check.. record is update 
			if(updateStmt.executeUpdate()==1){
			retailDetails.setMessage("success");
			retailDetails.setTid(tid);
			retailDetails.setAmount(amount);
			retailDetails.setBillingnumber(billingnumber);
			retailDetails.setRrn(rrn);
			}else{
				retailDetails.setMessage("Data mismatch or not found. Please check");
				retailDetails.setTid(tid);
				retailDetails.setAmount(amount);
				retailDetails.setBillingnumber(billingnumber);
				retailDetails.setRrn(rrn);
			}
		} catch (Exception e) {
			logger.error("Error in updateResponse method", e);
			retailDetails.setMessage("failure. please try again later.");
			retailDetails.setTid(tid);
			retailDetails.setAmount(amount);
			retailDetails.setBillingnumber(billingnumber);
			retailDetails.setRrn(rrn);
		} finally {
			try {
				if (updateStmt != null)
					updateStmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("failure. please try again later.", e);
			}
		}
		return retailDetails;
	}

	@Override
	public StatusDetails showstatusCheckNew(String tid, String amount, String billingnumber) {
		StatusDetails statusDetails = new StatusDetails();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		String status = "";
		String rrn = "";
		boolean isDataExits = false;
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall(
					"SELECT STATUS,RRN FROM RETAIL_TABLE_NEW WHERE TID=? AND AMOUNT=? AND BILLING_NUMBER=? ORDER BY INSERT_TIMESTAMP DESC LIMIT 1");

			pstmt.setString(1, tid);
			pstmt.setString(2, amount);
			pstmt.setString(3, billingnumber);
			resultSet = pstmt.executeQuery();
			
			while (resultSet.next()) {
				status = resultSet.getString("STATUS");
				rrn =resultSet.getString("RRN");
				isDataExits = true;
				break;
			}
			//if check data is not exits
			if(!isDataExits){
				statusDetails.setTid(tid);
				statusDetails.setAmount(amount);
				statusDetails.setBillingnumber(billingnumber);
				statusDetails.setRrn(rrn);
				statusDetails.setStatus("Data mismatch or Record Notfound");
			}else{			
			statusDetails.setTid(tid);
			statusDetails.setAmount(amount);
			statusDetails.setBillingnumber(billingnumber);
			statusDetails.setRrn(rrn);
			statusDetails.setStatus(status);
			}

		} catch (Exception e) {
			logger.error("statusCheck method TID=" + tid, e);
		} finally {
			try {
				if (pstmt != null && !pstmt.isClosed())
					pstmt.close();
				new DBConnectionProd().closeConnection(conn);
			} catch (SQLException e) {
				logger.error("fetchBillingStatus method TID=" + tid, e);
			}
		}
		return statusDetails;
	}

	public boolean checkIsRecordAlreadyAvailableNew(String tid) {

		Connection conn = null;
		PreparedStatement fetchProfileStmt = null;
		ResultSet result = null;
		boolean isRecordAvail = false;

		try {
			conn = new DBConnectionProd().getConnection(4);
			fetchProfileStmt = conn.prepareStatement("SELECT * FROM RETAIL_TABLE_NEW WHERE TID=?");
			fetchProfileStmt.setString(1, tid);
			result = fetchProfileStmt.executeQuery();

			while (result.next()) {
				isRecordAvail = true;
				break;
			}

		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailableNew]", e);
		}
		return isRecordAvail;

	}

	@Override
	public Object showdeletePaymentNew(String tid) {
		//delete payment if you press cancel in UI
		Connection conn = null;
		PreparedStatement pstmt = null;
		PaymentDetails details = new PaymentDetails();
		try {
			conn = new DBConnectionProd().getConnection(4);
			pstmt = conn.prepareCall("DELETE FROM RETAIL_TABLE_NEW WHERE TID=?");
			pstmt.setString(1, tid);
		    pstmt.executeUpdate();
		    details.setMessage("success");
		} catch (SQLException e) {
			logger.error("Error occurred in [SampleDaoImpl] [checkIsRecordAlreadyAvailable]", e);
		}
		return details;
	}

	/*
	 * @Override public ReturnObject blockingApi(String id, String model, String
	 * dealId, String serialNumber, String schemeId, String name, String
	 * imeiNumber, String flag) { Connection conn = null; PreparedStatement
	 * insertStmt = null; ReturnObject returnObject = new ReturnObject();
	 * 
	 * try { conn = new DBConnectionProd().getConnection(4); insertStmt =
	 * conn.prepareStatement(
	 * "INSERT INTO blocking_unblocking_table (ID, MODEL,DEAL_ID, SERIAL_NUMBER, SCHEME_ID, NAME, IMEI_NUMBER, FLAG) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
	 * );
	 * 
	 * insertStmt.setString(1, id); insertStmt.setString(2, model);
	 * insertStmt.setString(3, dealId); insertStmt.setString(4, serialNumber);
	 * insertStmt.setString(5, schemeId); insertStmt.setString(6, name);
	 * insertStmt.setString(7, imeiNumber); insertStmt.setString(8, flag);
	 * if(insertStmt.executeUpdate()==1){ returnObject.setReturnCode(0);
	 * returnObject.setReturnMessage("Success"); }
	 * 
	 * } catch (Exception e) { logger.error("Error in blockingApi method", e); }
	 * finally { try { if (insertStmt != null) insertStmt.close(); new
	 * DBConnectionProd().closeConnection(conn); } catch (SQLException e) {
	 * logger.error("Error in blockingApi method", e); } }
	 * 
	 * return returnObject; }
	 * 
	 * @Override public ReturnObject unblockingApi(String id, String flag) {
	 * Connection conn = null; PreparedStatement updateStmt = null; ReturnObject
	 * returnObject = new ReturnObject(); try { conn = new
	 * DBConnectionProd().getConnection(4); updateStmt = conn.prepareStatement(
	 * "UPDATE blocking_unblocking_table SET FLAG=? WHERE ID=? ");
	 * 
	 * updateStmt.setString(1, flag); updateStmt.setString(2, id);
	 * if(updateStmt.executeUpdate()==1){ returnObject.setReturnCode(0);
	 * returnObject.setReturnMessage("Success"); }
	 * 
	 * } catch (Exception e) { logger.error("Error in updateResponse method",
	 * e); } finally { try { if (updateStmt != null) updateStmt.close(); new
	 * DBConnectionProd().closeConnection(conn); } catch (SQLException e) {
	 * logger.error("failure. please try again later.", e); } } return
	 * returnObject; }
	 */

}
