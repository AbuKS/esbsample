package com.esb.sample.dao;

import java.util.ArrayList;

import com.esb.sample.model.InsertReqDetails;
import com.esb.sample.model.InsertResponseDetails;
import com.esb.sample.model.PaymentDetails;
import com.esb.sample.model.PaymentDetailsold;
import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;
import com.esb.sample.model.StatusDetails;

public interface SampleDaoNew {

	public InsertResponseDetails initiatePaymentNew(InsertReqDetails retailDetail);

	public InsertResponseDetails initiatePaymentType2(InsertReqDetails retailDetail);

	public ArrayList<ResDetailsForRetailType> fetchPaymentNew(String tid);

	public ResDetailsForRetailType updateResponseNew(RetailType2Details retailDetails);

	public ResDetailsForRetailType updateResponseType2(RetailType2Details retailDetails);

	public PaymentDetailsold showinitiatePaymentNew(String tid, String amount, String addntl_attr1, String addntl_atr2,
			String billingnumber, String rrn);

	public PaymentDetailsold showfetchPaymentNew(String tid);

	public PaymentDetailsold showupdateResponseNew(String tid, String amount, String response, String billingnumber,
			String addntl_attr1, String addntl_atr2, String rrn);

	public StatusDetails showstatusCheckNew(String tid, String amount, String billingnumber);

	boolean showupdateStatusNew(String tid, String amount, String response, String billingnumber, String addntl_attr1,
			String addntl_atr2);

	public Object showdeletePaymentNew(String tid);

	/*
	 * public ReturnObject blockingApi(String id, String model, String dealId,
	 * String serialNumber, String schemeId, String name, String imeiNumber,
	 * String flag);
	 * 
	 * public ReturnObject unblockingApi(String id, String flag);
	 */
}
