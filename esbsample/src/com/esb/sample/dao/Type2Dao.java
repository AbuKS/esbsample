package com.esb.sample.dao;

import com.esb.sample.model.ResDetailsForRetailType;
import com.esb.sample.model.RetailType2Details;

public interface Type2Dao {

	public RetailType2Details retailType2response(String tid,String amount);
	
	public RetailType2Details addUrnDetail(String tid,String amount);
	public ResDetailsForRetailType fetchUrnDetail(String urn);

}
